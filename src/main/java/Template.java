public class Template {
    String name;

    public String getServerPlugins() {
        return serverPlugins;
    }

    public String getClientPlugins() {
        return clientPlugins;
    }

    String serverPlugins;
    String clientPlugins;

    public Template(String key) {
        switch (key) {
            case "Defualt": {
                //name = "Defualt";
                serverPlugins = "xml-export.jar\n" +
                        "Workflow.jar\n" +
                        "ViewerCopies.jar\n" +
                        "UserData.jar\n" +
                        "usage-report.jar\n" +
                        "SavedSearch.jar\n" +
                        "reports.jar\n" +
                        "registration.jar\n" +
                        "PublicFileAccess.jar\n" +
                        "PublicationsOffline.jar\n" +
                        "MGOK.jar\n" +
                        "fund-report.jar\n" +
                        "FullPreviewFiles.jar\n" +
                        "fill-report.jar\n" +
                        "Favorites.jar\n" +
                        "FastInput.jar\n" +
                        "expiring-report.jar\n" +
                        "EbsConfiguration.jar\n" +
                        "Comments.jar\n" +
                        "CatalogueExport.jar\n" +
                        "BookTabs.jar\n";
                clientPlugins = "Workflow.jar\n" +
                        "PublicationsOffline.jar\n" +
                        "MGOK.jar\n" +
                        "FastInput.jar\n";
                break;
            }
            case "ebs": {
                //name = "ebs";
                serverPlugins = "BookTabs.jar\n" +
                        "Comments.jar\n" +
                        "DIPImport.jar\n" +
                        "EbsConfiguration.jar\n" +
                        "FastInput.jar\n" +
                        "Favorites.jar\n" +
                        "fill-report-server.jar\n" +
                        "FullPreviewFiles.jar\n" +
                        "fund-report-server.jar\n" +
                        "HelpPlugin.jar\n" +
                        "PublicFileAccess.jar\n" +
                        "registration-server.jar\n" +
                        "reports-server.jar\n" +
                        "SavedSearch.jar\n" +
                        "usage-report-server.jar\n" +
                        "UserData.jar\n" +
                        "ViewerCopies.jar\n";
                clientPlugins = "BookTabs.jar\n" +
                        "DIPImport.jar\n" +
                        "registration-client.jar\n" +
                        "SavedSearch.jar\n";
                break;
            }
            case "mchs": {
                //name = "mchs";
                serverPlugins = "BookTabs.jar\n" +
                        "Comments.jar\n" +
                        "EbsConfiguration.jar\n" +
                        "Favorites.jar\n" +
                        "fill-report-server.jar\n" +
                        "FullPreviewFiles.jar\n" +
                        "fund-report-server.jar\n" +
                        "HelpPlugin.jar\n" +
                        "PublicFileAccess.jar\n" +
                        "registration-server.jar\n" +
                        "reports-server.jar\n" +
                        "SavedSearch.jar\n" +
                        "synchronization-server.jar\n" +
                        "usage-report-server.jar\n" +
                        "UserData.jar\n" +
                        "ViewerCopies.jar\n";
                clientPlugins = "registration-client.jar";
                break;
            }
            case "helicopters": {
                //name = "helicopters";
                serverPlugins = "CatalogueExport.jar\n" +
                        "ConsideredCopies.jar\n" +
                        "custom-considered-copies-report-server.jar\n" +
                        "custom-nd-change-report-server.jar\n" +
                        "custom-subscriptions-report-server.jar\n" +
                        "Duplicates.jar\n" +
                        "expiring-report-server.jar\n" +
                        "linked-report-server.jar\n" +
                        "ND-subscribe.jar\n" +
                        "nd-changes-server.jar\n" +
                        "NormativeControl.jar\n" +
                        "PublicationsOffline.jar\n" +
                        "Reviews.jar\n" +
                        "stubfile-server.jar\n" +
                        "Voting.jar\n" +
                        "Workflow.jar\n" +
                        "xml-export-server.jar";
                clientPlugins = "            <clientPlugins>\n" +
                        "CatalogueExport.jar</string>\n" +
                        "ConsideredCopies.jar</string>\n" +
                        "Duplicates.jar</string>\n" +
                        "ND-subscribe.jar\n" +
                        "NormativeControl.jar\n" +
                        "PublicationsOffline.jar\n" +
                        "stubfile-client.jar\n" +
                        "Workflow.jar";
                break;
            }
            case "omk": {
                //name = "omk";
                serverPlugins ="Workflow.jar\n" +
                        "UserData.jar\n" +
                        "usage-report-server.jar\n" +
                        "SavedSearch.jar\n" +
                        "reports-server.jar\n" +
                        "PublicationsOffline.jar\n" +
                        "NormativeControl.jar\n" +
                        "ND-subscribe.jar\n" +
                        "fund-report-server.jar\n" +
                        "Favorites.jar\n" +
                        "expiring-report-server.jar\n" +
                        "EbsConfiguration.jar\n" +
                        "custom-subscriptions-report-server.jar\n" +
                        "custom-nd-change-report-server.jar\n" +
                        "custom-considered-copies-report-server.jar\n" +
                        "ConsideredCopies.jar\n" +
                        "Comments.jar\n" +
                        "CatalogueExport.jar\n" +
                        "BookTabs.jar\n";
                clientPlugins ="Workflow.jar\n" +
                        "SavedSearch.jar\n" +
                        "PublicationsOffline.jar\n" +
                        "NormativeControl.jar\n" +
                        "ND-subscribe.jar\n" +
                        "ConsideredCopies.jar\n" +
                        "CatalogueExport.jar\n" +
                        "BookTabs.jar\n";
                break;
            }
            case "cqms": {
                //name = "kbnti";
                serverPlugins = "CatalogueExport.jar\n" +
                        "ConsideredCopies.jar\n" +
                        "cqms-audits-server.jar\n" +
                        "cqms-fixation-base-crm-server.jar\n" +
                        "cqms-fixation-base-server.jar\n" +
                        "cqms-monitoring-server.jar\n" +
                        "cqms-risks-server.jar\n" +
                        "custom-considered-copies-report-server.jar\n" +
                        "custom-nd-change-report-server.jar\n" +
                        "custom-subscriptions-report-server.jar\n" +
                        "Duplicates.jar\n" +
                        "expiring-report-server.jar\n" +
                        "FastInput.jar\n" +
                        "linked-report-server.jar\n" +
                        "ND-subscribe.jar\n" +
                        "NormativeControl.jar\n" +
                        "PublicationsOffline.jar\n" +
                        "Workflow.jar\n" +
                        "xml-export-server.jar";
                clientPlugins = "CatalogueExport.jar\n" +
                        "ConsideredCopies.jar\n" +
                        "Duplicates.jar\n" +
                        "FastInput.jar\n" +
                        "MassInheritance.jar\n" +
                        "ND-subscribe.jar\n" +
                        "NormativeControl.jar\n" +
                        "PublicationsOffline.jar\n" +
                        "registration-client.jar\n" +
                        "Workflow.jar";
                break;
            }
            case "all_plugins": {
                //name = "";
                serverPlugins = "Actualization.jar\n" +
                        "archive-docs-server.jar\n" +
                        "BackupSpot.jar\n" +
                        "BookTabs.jar\n" +
                        "CatalogueExport.jar\n" +
                        "Comments.jar\n" +
                        "ConsideredCopies.jar\n" +
                        "cqms-audits-server.jar\n" +
                        "cqms-fixation-base-crm-server.jar\n" +
                        "cqms-fixation-base-server.jar\n" +
                        "cqms-monitoring-server.jar\n" +
                        "cqms-risks-server.jar\n" +
                        "custom-considered-copies-report-server.jar\n" +
                        "custom-nd-change-report-server.jar\n" +
                        "custom-subscriptions-report-server.jar\n" +
                        "DBMigration.jar\n" +
                        "DIPImport.jar\n" +
                        "DocumentReplacer.jar\n" +
                        "document-replacer-server.jar\n" +
                        "Duplicates.jar\n" +
                        "EbsConfiguration.jar\n" +
                        "expiring-report-server.jar\n" +
                        "FastInput.jar\n" +
                        "Favorites.jar\n" +
                        "FIASImport.jar\n" +
                        "fill-report-server.jar\n" +
                        "FullPreviewFiles.jar\n" +
                        "fund-report-server.jar\n" +
                        "HelpPlugin.jar\n" +
                        "kbntind-extension-server.jar\n" +
                        "linked-report-server.jar\n" +
                        "nd-changes-server.jar\n" +
                        "ND-subscribe.jar\n" +
                        "NormativeControl.jar\n" +
                        "NormdocsConverters.jar\n" +
                        "omk-extension-server.jar\n" +
                        "omk-wss-server.jar\n" +
                        "PublicationsOffline.jar\n" +
                        "PublicFileAccess.jar\n" +
                        "RatingPlugin.jar\n" +
                        "registration-server.jar\n" +
                        "reports-server.jar\n" +
                        "Reviews.jar\n" +
                        "RSSPlugin.jar\n" +
                        "SavedSearch.jar\n" +
                        "Selections.jar\n" +
                        "stubfile-server.jar\n" +
                        "synchronization-server.jar\n" +
                        "TestSystem.jar\n" +
                        "usage-report-server.jar\n" +
                        "UserData.jar\n" +
                        "ViewerCopies.jar\n" +
                        "ViewerLink.jar\n" +
                        "Voting.jar\n" +
                        "vtb24plugin-server.jar\n" +
                        "Workflow.jar\n" +
                        "xml-export-server.jar\n";
                clientPlugins = "Actualization.jar\n" +
                        "archive-docs-client.jar\n" +
                        "av-excel-import-client.jar\n" +
                        "BackupSpot.jar\n" +
                        "BookTabs.jar\n" +
                        "CatalogueExport.jar\n" +
                        "ConsideredCopies.jar\n" +
                        "DIPImport.jar\n" +
                        "Duplicates.jar\n" +
                        "FastInput.jar\n" +
                        "FoldersHierarchy.jar\n" +
                        "HelpPlugin.jar\n" +
                        "MassInheritance.jar\n" +
                        "MGOK.jar\n" +
                        "ND-subscribe.jar\n" +
                        "NormativeControl.jar\n" +
                        "NormdocsConverters.jar\n" +
                        "PublicationsOffline.jar\n" +
                        "RatingPlugin.jar\n" +
                        "registration-client.jar\n" +
                        "RSSPlugin.jar\n" +
                        "SavedSearch.jar\n" +
                        "Selections.jar\n" +
                        "stubfile-client.jar\n" +
                        "TestSystem.jar\n" +
                        "vtb24plugin-client.jar\n" +
                        "Workflow.jar\n";
                break;
            }
            case "manual": {
                clientPlugins = "";
                serverPlugins = "";
                //name = "";
                break;
            }
            case "kbnti" :{
                //name = "";
                serverPlugins = "Workflow.jar\n" +
                        "ViewerLink.jar\n" +
                        "ViewerCopies.jar\n" +
                        "UserData.jar\n" +
                        "usage-report-server.jar\n" +
                        "SavedSearch.jar\n" +
                        "reports-server.jar\n" +
                        "registration-server.jar\n" +
                        "PublicationsOffline.jar\n" +
                        "NormdocsConverters.jar\n" +
                        "NormativeControl.jar\n" +
                        "ND-subscribe.jar\n" +
                        "fund-report-server.jar\n" +
                        "fill-report-server.jar\n" +
                        "Favorites.jar\n" +
                        "FastInput.jar\n" +
                        "expiring-report-server.jar\n" +
                        "EbsConfiguration.jar\n" +
                        "custom-subscriptions-report-server.jar\n" +
                        "custom-nd-change-report-server.jar\n" +
                        "custom-considered-copies-report-server.jar\n" +
                        "ConsideredCopies.jar\n" +
                        "Comments.jar\n" +
                        "CatalogueExport.jar\n" +
                        "BookTabs.jar\n" +
                        "Actualization.jar\n";
                clientPlugins = "Workflow.jar\n" +
                        "SavedSearch.jar\n" +
                        "registration-client.jar\n" +
                        "PublicationsOffline.jar\n" +
                        "NormativeControl.jar\n" +
                        "ND-subscribe.jar\n" +
                        "MassInheritance.jar\n" +
                        "FastInput.jar\n" +
                        "ConsideredCopies.jar\n" +
                        "CatalogueExport.jar\n" +
                        "BookTabs.jar\n" +
                        "Actualization.jar\n";
                break;
            }
            default:{
                //name = "";
                serverPlugins = "";
                clientPlugins = "";
                break;
            }

        }
    }
}
