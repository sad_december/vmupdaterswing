package Workers;

import localUtils.FileTransfer;
import localUtils.fileUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.io.File;
import java.nio.file.Paths;
import java.util.Scanner;

public class ASWorker extends SwingWorker<Integer, String> {

    private static final Logger logger = LogManager.getLogger(ASWorker.class);

    private String downloadFolder;
    private String templateName;
    private String rootFolder1;
    private String hostName;
    private FileTransfer fileTransfer;
    private InfoFromTables info;
    private String buildVersion = "";
    private boolean featureTag = false;
    private final JTextArea messagesTextArea;

    public ASWorker(InfoFromTables infoFromTables, JTextArea messagesTextArea) {
        this.messagesTextArea = messagesTextArea;
        this.downloadFolder = System.getProperty("user.home") + "\\Downloads\\";
        this.templateName = infoFromTables.tempateName;
        this.info = infoFromTables;
        this.hostName = infoFromTables.hostname;
    }

    ASWorker(JTextArea messagesTextArea) {
        this.messagesTextArea = messagesTextArea;
        this.downloadFolder = System.getProperty("user.home") + "\\Downloads\\";
        this.templateName = "ArhiveServer";
    }

    @Override
    protected Integer doInBackground() throws Exception {
        //setName();
        unzipDownloaded();
        moveLocal();
        cleanViaTemplate();

        //setTestVmName();
        transfer();
        redeployExecute();
        return null;
    }

    private void redeployExecute() {
        if (info.synchroProxy) {
            fileTransfer.executeCommand(fileTransfer.executeCommand("/opt/glassfish4/glassfish/bin/asadmin --user admin -W /home/zForUpdateServer/password.txt deploy --force=true --name SynchronizationProxy /home/zForUpdateServer/SynchronizationProxy.war; rm -f file /home/zForUpdateServer//SynchronizationProxy.war"));
        }
        fileTransfer.executeCommand(fileTransfer.executeCommand("/opt/glassfish4/glassfish/bin/asadmin --user admin -W /home/zForUpdateServer/password.txt deploy --force=true --name ArchiveServer /home/zForUpdateServer/ArchiveServer.war; rm -f file /home/zForUpdateServer/ArchiveServer.war"));

        //fileTransfer.executeCommand("/opt/glassfish4/glassfish/bin/asadmin disable --user admin -W /home/zForUpdateServer/password.txt ArchiveServer");
        //fileTransfer.executeCommand("/opt/glassfish4/glassfish/bin/asadmin enable --user admin -W /home/zForUpdateServer/password.txt ArchiveServer");
        //fileTransfer.executeCommand("/etc/init.d/GlassFish_storm restart");

        fileTransfer.closeConnect();
        logger.info("Redeploying");
    }

    private void transfer() {
        fileTransfer = new FileTransfer();
        fileTransfer.sftpConnection(hostName);
        fileTransfer.executeCommand("rm -rf /home/storm/.alee/AleeArchiveServer3/plugins/*");
        fileTransfer.sendFileToServer(rootFolder1 + "ServerPlugins\\*.jar", "/home/storm/.alee/AleeArchiveServer3/plugins/");
        fileTransfer.sendFileToServer(rootFolder1 + "updates.zip", "/home/storm/.alee/AleeArchiveServer3/update/updates.zip");
        fileTransfer.sendFileToServer(rootFolder1 + "ArchiveServer.war", "/home/zForUpdateServer/ArchiveServer.war");
        fileTransfer.sendFileToServer(rootFolder1 + "asVersion.txt", "/home/zForUpdateServer/asVersion.txt");
        if (templateName.equals("mchs")) {
            fileTransfer.sendFileToServer(rootFolder1 + "SynchronizationProxy.war", "/home/zForUpdateServer/SynchronizationProxy.war");
        }
    }

    private void moveLocal() {
        fileUtil fileUtil = new fileUtil();
        fileUtil.copyFolder(rootFolder1 + "Files\\plugins\\client\\", rootFolder1 + "plugins\\");
        fileUtil.copyFolder(rootFolder1 + "Files\\plugins\\server\\", rootFolder1 + "ServerPlugins\\");
        fileUtil.copyFile(rootFolder1 + "Files\\installers\\updates.zip", rootFolder1 + "updates.zip");
        fileUtil.copyFile(rootFolder1 + "Files\\modules\\ArchiveServer.war", rootFolder1 + "ArchiveServer.war");
        fileUtil.copyFile(rootFolder1 + "Files\\modules\\SynchronizationProxy.war", rootFolder1 + "SynchronizationProxy.war");

        File asVersion = new File(rootFolder1 + "asVersion.txt");
        if (this.featureTag) {
            fileUtil.writer(asVersion, templateName + " feature " + buildVersion + "\n");
        } else {
            fileUtil.writer(asVersion, templateName + " " + buildVersion + "\n");
        }

        fileUtil.deleteDirectory(new File(rootFolder1 + "Files\\"));
    }

    private void cleanViaTemplate() {
        fileUtil fileUtil = new fileUtil();
        fileUtil.deleteFiles(rootFolder1 + "ServerPlugins\\", info.serverPlugins);
        fileUtil.deleteFiles(rootFolder1 + "plugins\\", info.clientPlugins);
        fileUtil.addToZip(rootFolder1 + "updates.zip", rootFolder1 + "plugins\\");
    }

    private void unzipDownloaded() {
        fileUtil fileUtil = new fileUtil();
        File downloadedZip = fileUtil.getMostRecentFile(Paths.get(downloadFolder), "Stor-M_3");
        if (downloadedZip.getName().contains("Feature")) {
            buildVersion = downloadedZip.getName().substring(17, 21);
            this.featureTag = true;
        } else {
            buildVersion = downloadedZip.getName().substring(9, 13);
        }
        String rootFolder = System.getProperty("user.home") + "\\vmUpdater\\" + templateName + "\\" + buildVersion + "\\";
        rootFolder1 = rootFolder;
        try {
            new fileUtil().deleteDirectory(new File(rootFolder1));
            logger.info("Old files in temp deleted");
        } catch (Exception ignored) {
        }
        fileUtil.createFolder(rootFolder);
        fileUtil.createFolder(rootFolder + "plugins\\");
        fileUtil.createFolder(rootFolder + "ServerPlugins\\");
        fileUtil.unzip(downloadedZip.getAbsolutePath(), rootFolder);
    }
}
