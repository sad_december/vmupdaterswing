package Workers;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import localUtils.FileTransfer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.util.Objects;

public class VMControlWorker extends SwingWorker<Integer, String> {

    private static final Logger logger = LogManager.getLogger(ASWorker.class);
    private String hostName;
    private FileTransfer fileTransfer;
    private int iDoperation;
    private JTextArea jTextArea;
    private JLabel jLabel;
    private JTextArea jTextAreaVmStatus;

    public VMControlWorker(String hostName, int iDoperation) {
        this.iDoperation = iDoperation;
        this.hostName = hostName;
    }

    public VMControlWorker(String hostName, int iDoperation, JLabel jLabel, JTextArea jTextArea) {
        this.jLabel = jLabel;
        this.jTextAreaVmStatus = jTextArea;
        this.iDoperation = iDoperation;
        this.hostName = hostName;
    }

    public VMControlWorker(String hostName, int iDoperation, JTextArea jTextArea) {
        this.iDoperation = iDoperation;
        this.hostName = hostName;
        this.jTextArea = jTextArea;
    }

    public void closeConnection() {
        try {
            fileTransfer.closeConnect();
        } catch (Exception ignored) {
        }
    }

    @Override
    protected Integer doInBackground() {
        fileTransfer = new FileTransfer();
        fileTransfer.sftpConnection(hostName);

        switch (iDoperation) {
            case 0: {
                fileTransfer.executeCommand("/etc/init.d/GlassFish_storm start");
                break;
            }
            case 1: {
                fileTransfer.executeCommand("/etc/init.d/GlassFish_storm stop");
                break;
            }
            case 2: {
                fileTransfer.executeCommand("/etc/init.d/GlassFish_storm restart");
                break;
            }
            case 3: {
                fileTransfer.executeCommand("tail -f -n 4000 /opt/glassfish4/glassfish/domains/storm/logs/server.log", jTextArea);
                break;
            }
            case 4: {
                fileTransfer.executeCommand("pkill -9 java");
                break;
            }
            case 5: {
                try {
                    fileTransfer.checkConnection();
                    jLabel.setText("Online");
                    StringBuilder stringBuffer = new StringBuilder();
                    stringBuffer
                            .append("AS :")
                            .append(fileTransfer.getInfoFormFile("/home/zForUpdateServer/asVersion.txt"))
                            .append("WEB :")
                            .append(fileTransfer.getInfoFormFile("/home/zForUpdateServer/webVersion.txt"))
                            .append("db :\n")
                            .append(fileTransfer.getBase())
                            .append("GF apps: \n")
                            .append(fileTransfer.getGlassfishApps());
                    jTextAreaVmStatus.setText(stringBuffer.substring(0, stringBuffer.indexOf("Command"))); //.substring(0, stringBuffer.indexOf("Command")
                } catch (JSchException e) {
                    jLabel.setText("Offline");
                    jTextAreaVmStatus.setText("No info");
                    break;
                }
            }
            default: {
                break;
            }
        }
        fileTransfer.closeConnect();
        return 1;
    }
}
