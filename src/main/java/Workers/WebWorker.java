package Workers;

import localUtils.FileTransfer;
import localUtils.fileUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.io.File;
import java.nio.file.Paths;

public class WebWorker extends SwingWorker<Integer, String> {
    private static final Logger logger = LogManager.getLogger(ASWorker.class);

    private String downloadFolder;
    private String templateName;
    private String rootFolder1;
    private fileUtil fileUtil;
    private String hostName;
    private FileTransfer fileTransfer;
    private String webAppName;
    private boolean featureTag = false;
    private final JTextArea messagesTextArea;
    private String buildVersion = "";

    public WebWorker(InfoFromTables infoFromTables, JTextArea messagesTextArea) {
        this.downloadFolder = System.getProperty("user.home") + "\\Downloads\\";
        this.templateName = infoFromTables.webTemplateName;
        this.webAppName = infoFromTables.webAppName;
        this.hostName = infoFromTables.hostname;
        this.messagesTextArea = messagesTextArea;
    }

    public WebWorker() {
        this.downloadFolder = System.getProperty("user.home") + "\\Downloads\\";
        this.templateName = "Default";
        messagesTextArea = null;
    }

    @Override
    protected Integer doInBackground() throws Exception {
        unzipDownloaded();
        moveLocal();
        transfer();
        executeRedeploy();
        return 1;
    }

    private void executeRedeploy() {
        fileTransfer.sftpConnection(hostName);
        fileTransfer.executeCommand("mkdir -p /home/storm/.alee/storm3web/" + templateName + "/");
        fileTransfer.executeCommand("rm -rf /home/storm/.alee/storm3web/" + templateName + "/fonts");
        fileTransfer.executeCommand("rm -rf /home/storm/.alee/storm3web/" + templateName + "/images");
        fileTransfer.executeCommand("rm -rf /home/storm/.alee/storm3web/" + templateName + "/language");
        fileTransfer.executeCommand("rm -rf /home/storm/.alee/storm3web/" + templateName + "/markup");
        fileTransfer.executeCommand("rm -rf /home/storm/.alee/storm3web/" + templateName + "/plugins");
        fileTransfer.executeCommand("rm -rf /home/storm/.alee/storm3web/" + templateName + "/styles");
        fileTransfer.executeCommand("rm -rf /home/storm/.alee/storm3web/" + templateName + "/templates");
        fileTransfer.executeCommand("rm -rf /home/storm/.alee/storm3web/" + templateName + "/widgets");
        fileTransfer.executeCommand("unzip -o /home/zForUpdateServer/" + templateName + ".zip -d /home/storm/.alee/storm3web/" + templateName + "/");

        fileTransfer.executeCommand("/opt/glassfish4/glassfish/bin/asadmin --user admin -W /home/zForUpdateServer/password.txt deploy --force=true --name " + webAppName + " /home/zForUpdateServer/StormWebApplication.war; rm -f file /home/zForUpdateServer/StormWebApplication.war");
        fileTransfer.executeCommand("/opt/glassfish4/glassfish/bin/asadmin --user admin -W /home/zForUpdateServer/password.txt disable " + webAppName);
        fileTransfer.executeCommand("/opt/glassfish4/glassfish/bin/asadmin --user admin -W /home/zForUpdateServer/password.txt enable " + webAppName);

        fileTransfer.closeConnect();
        logger.info("Redeploying");
    }

    private void transfer() {
        fileTransfer = new FileTransfer();
        fileTransfer.sftpConnection(hostName);
        fileTransfer.sendFileToServer(rootFolder1 + "StormWebApplication.war", "/home/zForUpdateServer/StormWebApplication.war");
        fileTransfer.sendFileToServer(rootFolder1 + templateName + ".zip", "/home/zForUpdateServer/" + templateName + ".zip");
        fileTransfer.sendFileToServer(rootFolder1 + "webVersion.txt", "/home/zForUpdateServer/webVersion.txt");
    }

    private void moveLocal() {
        fileUtil.copyFile(rootFolder1 + "Files\\StormWebApplication.war", rootFolder1 + "StormWebApplication.war");
        fileUtil.copyFile(rootFolder1 + "Files\\Templates\\" + templateName + ".zip", rootFolder1 + templateName + ".zip");
        fileUtil.deleteDirectory(new File(rootFolder1 + "Files"));

        fileUtil.createFolder(rootFolder1 + "Files\\WEB-INF\\");
        File settings = new File(rootFolder1 + "Files\\WEB-INF\\Settings.location");
        fileUtil.writer(settings, "/home/storm/.alee/storm3web/" + templateName + "/");
        fileUtil.addToZip(rootFolder1 + "StormWebApplication.war", rootFolder1 + "Files\\WEB-INF\\");

        File storm3web = new File(rootFolder1 + "storm3web.xml");
        fileUtil.writer(storm3web, "<ApplicationPath>/home/storm/.alee/storm3web/" + templateName + "/</ApplicationPath> ");
        fileUtil.addFileToZip(rootFolder1 + templateName + ".zip", storm3web.getAbsolutePath());

        File webVersion = new File(rootFolder1 + "webVersion.txt");
        if (featureTag) {
            fileUtil.writer(webVersion, templateName + " feature " + buildVersion + "\n");
        } else {
            fileUtil.writer(webVersion, templateName + " " + buildVersion + "\n");
        }
        fileUtil.deleteDirectory(new File(rootFolder1 + "Files"));
    }

    private void unzipDownloaded() {
        fileUtil = new fileUtil();
        File downloadedZip = fileUtil.getMostRecentFile(Paths.get(downloadFolder), "Stor-M_Web");
        if (downloadedZip.getName().contains("Feature")) {
            featureTag = true;
            buildVersion = downloadedZip.getName().substring(35, 39);
        } else {
            buildVersion = downloadedZip.getName().substring(27, 31);
        }
        String rootFolder = System.getProperty("user.home") + "\\vmUpdater\\WEB_" + templateName + "\\" + buildVersion + "\\";
        rootFolder1 = rootFolder;
        fileUtil.createFolder(rootFolder);
        fileUtil.unzip(downloadedZip.getAbsolutePath(), rootFolder);
    }
}