import Workers.InfoFromTables;
import Workers.VMControlWorker;
import Workers.WebWorker;
import Workers.ASWorker;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import localUtils.fileUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintStream;
import java.util.Enumeration;

public class AppFrom extends JFrame {

    private static final Logger logger = LogManager.getLogger(AppFrom.class);

    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    private JPanel arhiveServerPanel;
    private JPanel webAppPanel;
    private JPanel logPanel1;

    //ARCHIVE SERVER CONFIG
    private JButton defaultButton;
    private JButton ebsButton;
    private JButton mchsButton;
    private JButton helicoptersButton;
    private JButton OMKButton;
    private JButton CQMSButton;
    private JButton KBNTIButton;
    private JButton allPluginsButton;
    private JButton clearPluginsButton;

    //SERVER PLUGIN BOXES
    private JCheckBox actualizationCheckBox;
    private JCheckBox archiveDocsServerCheckBox;
    private JCheckBox bookTabsCheckBox;
    private JCheckBox catalogueExportCheckBox;
    private JCheckBox commentsCheckBox;
    private JCheckBox consideredCopiesCheckBox;
    private JCheckBox cqmsAuditsServerCheckBox;
    private JCheckBox cqmsFixationBaseCrmCheckBox;
    private JCheckBox cqmsFixationBaseServerCheckBox;
    private JCheckBox cqmsMonitoringServerCheckBox;
    private JCheckBox customConsideredCopiesReportCheckBox;
    private JCheckBox customNdChangeReportCheckBox;
    private JCheckBox customSubscriptionsReportServerCheckBox;
    private JCheckBox DBMigrationCheckBox;
    private JCheckBox DIPImportCheckBox;
    private JCheckBox documentReplacerCheckBox;
    private JCheckBox documentReplacerServerCheckBox;
    private JCheckBox duplicatesCheckBox;
    private JCheckBox ebsConfigurationCheckBox;
    private JCheckBox expiringReportServerCheckBox;
    private JCheckBox fastInputCheckBox;
    private JCheckBox favoritesCheckBox;
    private JCheckBox FIASImportCheckBox;
    private JCheckBox fillReportServerCheckBox;
    private JCheckBox fullPreviewFilesCheckBox;
    private JCheckBox fundReportServerCheckBox;
    private JCheckBox helpPluginCheckBox;
    private JCheckBox kbntindExtensionServerCheckBox;
    private JCheckBox linkedReportServerCheckBox;
    private JCheckBox ndChangesServerCheckBox;
    private JCheckBox NDSubscribeCheckBox;
    private JCheckBox normativeControlCheckBox;
    private JCheckBox normdocsConvertersCheckBox;
    private JCheckBox omkExtensionServerCheckBox;
    private JCheckBox omkWssServerCheckBox;
    private JCheckBox publicationsOfflineCheckBox;
    private JCheckBox publicFileAccessCheckBox;
    private JCheckBox ratingPluginCheckBox;
    private JCheckBox registrationServerCheckBox;
    private JCheckBox reportsServerCheckBox;
    private JCheckBox reviewsCheckBox;
    private JCheckBox RSSPluginCheckBox;
    private JCheckBox savedSearchCheckBox;
    private JCheckBox selectionsCheckBox;
    private JCheckBox stubfileServerCheckBox;
    private JCheckBox synchronizationServerCheckBox;
    private JCheckBox testSystemCheckBox;
    private JCheckBox usageReportServerCheckBox;
    private JCheckBox userDataCheckBox;
    private JCheckBox viewerCopiesCheckBox;
    private JCheckBox viewerLinkCheckBox;
    private JCheckBox votingCheckBox;
    private JCheckBox vtb24pluginServerCheckBox;
    private JCheckBox workflowCheckBox;

    //CLIENT PLUGIN BOXES
    private JCheckBox xmlExportServerCheckBox;
    private JCheckBox actualizationCheckBox1;
    private JCheckBox archiveDocsClientCheckBox;
    private JCheckBox avExcelImportClientCheckBox;
    private JCheckBox backupSpotCheckBox;
    private JCheckBox catalogueExportCheckBox1;
    private JCheckBox consideredCopiesCheckBox1;
    private JCheckBox DIPImportCheckBox1;
    private JCheckBox duplicatesCheckBox1;
    private JCheckBox fastInputCheckBox1;
    private JCheckBox foldersHierarchyCheckBox;
    private JCheckBox helpPluginCheckBox1;
    private JCheckBox massInheritanceCheckBox;
    private JCheckBox MGOKCheckBox;
    private JCheckBox NDSubscribeCheckBox1;
    private JCheckBox normativeControlCheckBox1;
    private JCheckBox publicationsOfflineCheckBox1;
    private JCheckBox ratingPluginCheckBox1;
    private JCheckBox registrationClientCheckBox;
    private JCheckBox RSSPluginCheckBox1;
    private JCheckBox savedSearchCheckBox1;
    private JCheckBox selectionsCheckBox1;
    private JCheckBox stubfileClientCheckBox;
    private JCheckBox testSystemCheckBox1;
    private JCheckBox vtb24pluginClientCheckBox;
    private JCheckBox workflowCheckBox1;

    //TEMPATES RADIOBUTTONS
    private JRadioButton defaultRadioButton;
    private JRadioButton EBSTouchRadioButton;
    private JRadioButton EBSRadioButton;
    private JRadioButton MCHSTouchRadioButton;
    private JRadioButton MCHSRadioButton;
    private JRadioButton russianHelicoptersRadioButton;
    private JRadioButton OMKRadioButton;
    private JRadioButton KBNTIRadioButton;
    private JRadioButton KSMKRadioButton;
    private JRadioButton normdocsCatalogRadioButton;
    private JRadioButton KBNTI2RadioButton;
    private JRadioButton KBNTINDRadioButton;

    //APP NAME IN GLASSFISH
    private JRadioButton stormWebApplicationRadioButton;
    private JRadioButton stormWebApplicationTouchRadioButton;
    private JRadioButton KSMKRadioButton1;
    private JRadioButton KBNTINDRadioButton1;
    private JButton deployWebApplicationButton;
    private JButton deployArhiveServerButton;

    //BUTTONS TO DEPLOY
    private JTextArea здесьНемногоИнфыПроTextArea;
    private JTextArea здесьМногоИнфыПроTextArea;
    private JPanel panel2;
    private JPanel Deploy;
    private JPanel webAppInfo;
    private JPanel arhiveServerInfo;
    private JCheckBox backupSpotCheckBoxServer;
    private JPanel serverPluginsPanel;
    private JPanel clientPluginsPanel;
    private JCheckBox bookTabsCheckBox1;
    private JCheckBox cqmsRisksServerCheckBox;
    private JRadioButton testvmRadioButton;
    private JRadioButton testvm3RadioButton;
    private JRadioButton testvm6RadioButton;
    private JRadioButton testvm7RadioButton;
    private JRadioButton testvm10RadioButton;
    private JRadioButton testvm11RadioButton;
    private JRadioButton testvm12RadioButton;
    private JRadioButton testvm13RadioButton;
    private JRadioButton testvm14RadioButton;
    private JTextArea textArea1;
    private JCheckBox normdocsConvertersCheckBox1;
    private JButton cleanTempButton;
    private JPanel panel3;
    private JButton stopButton;
    private JButton startButton;
    private JButton restartButton;
    private JButton showLogButton;
    private JButton cleanButton;
    private JButton stopLogButton;
    private JButton killJavaButton;
    private JButton refreshButton;
    private JPanel vmInfo;
    private JLabel testvmStatus;
    private JLabel testvm3Status;
    private JLabel testvm6Status;
    private JLabel testvm7Status;
    private JLabel testvm10Status;
    private JLabel testvm11Status;
    private JLabel testvm12Status;
    private JLabel testvm13Status;
    private JLabel testvm14Status;
    private JButton refreshButton1;
    private JTextArea textAreaTestVmStatus;
    private JTextArea textAreaTestVm3Status;
    private JTextArea textAreaTestVm6Status;
    private JTextArea textAreaTestVm7Status;
    private JTextArea textAreaTestVm10Status;
    private JTextArea textAreaTestVm11Status;
    private JTextArea textAreaTestVm12Status;
    private JTextArea textAreaTestVm13Status;
    private JTextArea textAreaTestVm14Status;
    private ButtonGroup hostName;
    private VMControlWorker jTextLoger;
    private ButtonGroup templates;
    private ButtonGroup webAppName;


    //Log stream
    private PrintStream standardOut;
    private String asConfName = "emptyName";

    public AppFrom() {
        this.getContentPane().add(panel1);
        redirectCOUT();
        addListeners();
        serverControlButtons();
        infoGrabber();
    }

    private void infoGrabber() {
        tabbedPane1.addChangeListener(new ChangeListener() {
            /**
             * Invoked when the target of the listener has changed its state.
             *
             * @param e a ChangeEvent object
             */
            @Override
            public void stateChanged(ChangeEvent e) {
                if (e.getSource() instanceof JTabbedPane) {
                    JTabbedPane tempPane = (JTabbedPane) e.getSource();
                    if (tempPane.getSelectedIndex() == 4) {
                        testvmStatus.setText("Checking");
                        VMControlWorker vmControlWorker = new VMControlWorker("testvm", 5, testvmStatus, textAreaTestVmStatus);
                        vmControlWorker.execute();

                        testvm3Status.setText("Checking");
                        VMControlWorker vmControlWorker3 = new VMControlWorker("testvm3", 5, testvm3Status, textAreaTestVm3Status);
                        vmControlWorker3.execute();

                        testvm6Status.setText("Checking");
                        VMControlWorker vmControlWorker6 = new VMControlWorker("testvm6", 5, testvm6Status, textAreaTestVm6Status);
                        vmControlWorker6.execute();

                        testvm7Status.setText("Checking");
                        VMControlWorker vmControlWorker7 = new VMControlWorker("testvm7", 5, testvm7Status, textAreaTestVm7Status);
                        vmControlWorker7.execute();

                        testvm10Status.setText("Checking");
                        VMControlWorker vmControlWorker10 = new VMControlWorker("testvm10", 5, testvm10Status, textAreaTestVm10Status);
                        vmControlWorker10.execute();

                        testvm11Status.setText("Checking");
                        VMControlWorker vmControlWorker11 = new VMControlWorker("testvm11", 5, testvm11Status, textAreaTestVm11Status);
                        vmControlWorker11.execute();

                        testvm12Status.setText("Checking");
                        VMControlWorker vmControlWorker12 = new VMControlWorker("testvm12", 5, testvm12Status, textAreaTestVm12Status);
                        vmControlWorker12.execute();

                        testvm13Status.setText("Checking");
                        VMControlWorker vmControlWorker13 = new VMControlWorker("testvm13", 5, testvm13Status, textAreaTestVm13Status);
                        vmControlWorker13.execute();

                        testvm14Status.setText("Checking");
                        VMControlWorker vmControlWorker14 = new VMControlWorker("testvm14", 5, testvm14Status, textAreaTestVm14Status);
                        vmControlWorker14.execute();
                    }
                    if (tempPane.getSelectedIndex() == 2) {
                        InfoFromTables info1 = getInfo();
                        здесьМногоИнфыПроTextArea.setText(new StringBuffer()
                                .append("AS Configuration: ")
                                .append(asConfName)
                                .append("\n\nServer plugins:\n")
                                .append(info1.serverPlugins)
                                .append("\nClient plugins: \n")
                                .append(info1.clientPlugins)
                                .toString());
                        здесьНемногоИнфыПроTextArea.setText(new StringBuffer()
                                .append("Web Template: ")
                                .append(info1.webTemplateName)
                                .append("\nApp name: ")
                                .append(info1.webAppName)
                                .toString());
                    }
                }
            }
        });
        refreshButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                testvmStatus.setText("Checking");
                VMControlWorker vmControlWorker = new VMControlWorker("testvm", 5, testvmStatus, textAreaTestVmStatus);
                vmControlWorker.execute();

                testvm3Status.setText("Checking");
                VMControlWorker vmControlWorker3 = new VMControlWorker("testvm3", 5, testvm3Status, textAreaTestVm3Status);
                vmControlWorker3.execute();

                testvm6Status.setText("Checking");
                VMControlWorker vmControlWorker6 = new VMControlWorker("testvm6", 5, testvm6Status, textAreaTestVm6Status);
                vmControlWorker6.execute();

                testvm7Status.setText("Checking");
                VMControlWorker vmControlWorker7 = new VMControlWorker("testvm7", 5, testvm7Status, textAreaTestVm7Status);
                vmControlWorker7.execute();

                testvm10Status.setText("Checking");
                VMControlWorker vmControlWorker10 = new VMControlWorker("testvm10", 5, testvm10Status, textAreaTestVm10Status);
                vmControlWorker10.execute();

                testvm11Status.setText("Checking");
                VMControlWorker vmControlWorker11 = new VMControlWorker("testvm11", 5, testvm11Status, textAreaTestVm11Status);
                vmControlWorker11.execute();

                testvm12Status.setText("Checking");
                VMControlWorker vmControlWorker12 = new VMControlWorker("testvm12", 5, testvm12Status, textAreaTestVm12Status);
                vmControlWorker12.execute();

                testvm13Status.setText("Checking");
                VMControlWorker vmControlWorker13 = new VMControlWorker("testvm13", 5, testvm13Status, textAreaTestVm13Status);
                vmControlWorker13.execute();

                testvm14Status.setText("Checking");
                VMControlWorker vmControlWorker14 = new VMControlWorker("testvm14", 5, testvm14Status, textAreaTestVm14Status);
                vmControlWorker14.execute();
            }
        });
    }

    private void serverControlButtons() {
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                VMControlWorker vmControlWorker = new VMControlWorker(getSelectedButtonText(hostName), 1);
                vmControlWorker.execute();
            }
        });
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                VMControlWorker vmControlWorker = new VMControlWorker(getSelectedButtonText(hostName), 0);
                vmControlWorker.execute();
            }
        });
        restartButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                VMControlWorker vmControlWorker = new VMControlWorker(getSelectedButtonText(hostName), 2);
                vmControlWorker.execute();
            }
        });
        showLogButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jTextLoger != null) {
                    jTextLoger.closeConnection();
                    jTextLoger.cancel(true);
                }
                textArea1.setText("");
                jTextLoger = new VMControlWorker(getSelectedButtonText(hostName), 3, textArea1);
                jTextLoger.execute();
                tabbedPane1.setSelectedIndex(3);
            }
        });
        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jTextLoger != null) {
                    jTextLoger.closeConnection();
                    jTextLoger.cancel(true);
                }
                textArea1.setText("");
                jTextLoger = new VMControlWorker(getSelectedButtonText(hostName), 3, textArea1);
                jTextLoger.execute();
            }
        });
        stopLogButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jTextLoger != null) {
                    jTextLoger.closeConnection();
                    jTextLoger.cancel(true);
                }
            }
        });
        cleanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    textArea1.setText("");
                } catch (Exception ignored) {
                }
            }
        });
        killJavaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                VMControlWorker vmControlWorker = new VMControlWorker(getSelectedButtonText(hostName), 4);
                vmControlWorker.execute();
            }
        });
    }

    private void addListeners() {
        //Plugins conf
        defaultButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                asConfName = "Default";

                //Clear all checkboxes
                boxCleaner(serverPluginsPanel, false);
                boxCleaner(clientPluginsPanel, false);

                //Server plugins conf
                xmlExportServerCheckBox.setSelected(true);
                workflowCheckBox.setSelected(true);
                viewerLinkCheckBox.setSelected(true);
                viewerCopiesCheckBox.setSelected(true);
                userDataCheckBox.setSelected(true);
                usageReportServerCheckBox.setSelected(true);
                savedSearchCheckBox.setSelected(true);
                bookTabsCheckBox.setSelected(true);
                catalogueExportCheckBox.setSelected(true);
                commentsCheckBox.setSelected(true);
                duplicatesCheckBox.setSelected(true);
                ebsConfigurationCheckBox.setSelected(true);
                expiringReportServerCheckBox.setSelected(true);
                fastInputCheckBox.setSelected(true);
                favoritesCheckBox.setSelected(true);
                fillReportServerCheckBox.setSelected(true);
                fundReportServerCheckBox.setSelected(true);
                publicationsOfflineCheckBox.setSelected(true);
                registrationServerCheckBox.setSelected(true);
                reportsServerCheckBox.setSelected(true);

                //Client plugins conf
                workflowCheckBox1.setSelected(true);
                publicationsOfflineCheckBox1.setSelected(true);
                MGOKCheckBox.setSelected(true);
                fastInputCheckBox1.setSelected(true);
                savedSearchCheckBox1.setSelected(true);
                catalogueExportCheckBox1.setSelected(true);
                duplicatesCheckBox1.setSelected(true);
                massInheritanceCheckBox.setSelected(true);
                registrationClientCheckBox.setSelected(true);

            }
        });
        ebsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                asConfName = "ebs";

                //Clear all checkboxes
                boxCleaner(serverPluginsPanel, false);
                boxCleaner(clientPluginsPanel, false);

                //Server plugins conf
                bookTabsCheckBox.setSelected(true);
                commentsCheckBox.setSelected(true);
                DIPImportCheckBox.setSelected(true);
                ebsConfigurationCheckBox.setSelected(true);
                fastInputCheckBox.setSelected(true);
                favoritesCheckBox.setSelected(true);
                fillReportServerCheckBox.setSelected(true);
                fullPreviewFilesCheckBox.setSelected(true);
                fundReportServerCheckBox.setSelected(true);
                helpPluginCheckBox.setSelected(true);
                publicFileAccessCheckBox.setSelected(true);
                registrationServerCheckBox.setSelected(true);
                reportsServerCheckBox.setSelected(true);
                savedSearchCheckBox.setSelected(true);
                usageReportServerCheckBox.setSelected(true);
                userDataCheckBox.setSelected(true);
                viewerCopiesCheckBox.setSelected(true);

                //Client plugins conf
                bookTabsCheckBox1.setSelected(true);
                DIPImportCheckBox1.setSelected(true);
                registrationClientCheckBox.setSelected(true);
                savedSearchCheckBox1.setSelected(true);
                publicationsOfflineCheckBox1.setSelected(true);
            }
        });
        mchsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                asConfName = "mchs";

                //Clear all checkboxes
                boxCleaner(serverPluginsPanel, false);
                boxCleaner(clientPluginsPanel, false);

                //Server plugins conf
                bookTabsCheckBox.setSelected(true);
                commentsCheckBox.setSelected(true);
                ebsConfigurationCheckBox.setSelected(true);
                favoritesCheckBox.setSelected(true);
                fillReportServerCheckBox.setSelected(true);
                fullPreviewFilesCheckBox.setSelected(true);
                fundReportServerCheckBox.setSelected(true);
                helpPluginCheckBox.setSelected(true);
                publicFileAccessCheckBox.setSelected(true);
                registrationServerCheckBox.setSelected(true);
                reportsServerCheckBox.setSelected(true);
                savedSearchCheckBox.setSelected(true);
                synchronizationServerCheckBox.setSelected(true);
                usageReportServerCheckBox.setSelected(true);
                userDataCheckBox.setSelected(true);
                viewerCopiesCheckBox.setSelected(true);

                //Client plugins conf
                registrationClientCheckBox.setSelected(true);
            }
        });
        helicoptersButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                asConfName = "helicopters";

                //Clear all checkboxes
                boxCleaner(serverPluginsPanel, false);
                boxCleaner(clientPluginsPanel, false);

                //Server plugins conf
                bookTabsCheckBox.setSelected(true);
                catalogueExportCheckBox.setSelected(true);
                commentsCheckBox.setSelected(true);
                consideredCopiesCheckBox.setSelected(true);
                customConsideredCopiesReportCheckBox.setSelected(true);
                customNdChangeReportCheckBox.setSelected(true);
                customSubscriptionsReportServerCheckBox.setSelected(true);
                duplicatesCheckBox.setSelected(true);
                ebsConfigurationCheckBox.setSelected(true);
                expiringReportServerCheckBox.setSelected(true);
                fastInputCheckBox.setSelected(true);
                favoritesCheckBox.setSelected(true);
                linkedReportServerCheckBox.setSelected(true);
                fillReportServerCheckBox.setSelected(true);
                fundReportServerCheckBox.setSelected(true);
                ndChangesServerCheckBox.setSelected(true);
                NDSubscribeCheckBox.setSelected(true);
                ndChangesServerCheckBox.setSelected(true);
                normativeControlCheckBox.setSelected(true);
                publicationsOfflineCheckBox.setSelected(true);
                reportsServerCheckBox.setSelected(true);
                reviewsCheckBox.setSelected(true);
                savedSearchCheckBox.setSelected(true);
                stubfileServerCheckBox.setSelected(true);
                usageReportServerCheckBox.setSelected(true);
                userDataCheckBox.setSelected(true);
                viewerCopiesCheckBox.setSelected(true);
                viewerLinkCheckBox.setSelected(true);
                votingCheckBox.setSelected(true);
                workflowCheckBox.setSelected(true);
                xmlExportServerCheckBox.setSelected(true);

                //Client plugins conf
                bookTabsCheckBox1.setSelected(true);
                catalogueExportCheckBox1.setSelected(true);
                consideredCopiesCheckBox1.setSelected(true);
                duplicatesCheckBox1.setSelected(true);
                fastInputCheckBox1.setSelected(true);
                massInheritanceCheckBox.setSelected(true);
                NDSubscribeCheckBox1.setSelected(true);
                normativeControlCheckBox1.setSelected(true);
                publicationsOfflineCheckBox1.setSelected(true);
                savedSearchCheckBox1.setSelected(true);
                stubfileClientCheckBox.setSelected(true);
                workflowCheckBox1.setSelected(true);
            }
        });
        OMKButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                asConfName = "OMK";

                //Clear all checkboxes
                boxCleaner(serverPluginsPanel, false);
                boxCleaner(clientPluginsPanel, false);

                //Server plugins conf
                workflowCheckBox.setSelected(true);
                userDataCheckBox.setSelected(true);
                usageReportServerCheckBox.setSelected(true);
                savedSearchCheckBox.setSelected(true);
                reportsServerCheckBox.setSelected(true);
                publicationsOfflineCheckBox.setSelected(true);
                normativeControlCheckBox.setSelected(true);
                NDSubscribeCheckBox.setSelected(true);
                fundReportServerCheckBox.setSelected(true);
                favoritesCheckBox.setSelected(true);
                expiringReportServerCheckBox.setSelected(true);
                ebsConfigurationCheckBox.setSelected(true);
                customSubscriptionsReportServerCheckBox.setSelected(true);
                customNdChangeReportCheckBox.setSelected(true);
                customConsideredCopiesReportCheckBox.setSelected(true);
                consideredCopiesCheckBox.setSelected(true);
                commentsCheckBox.setSelected(true);
                catalogueExportCheckBox.setSelected(true);
                bookTabsCheckBox.setSelected(true);

                //Client plugins conf
                workflowCheckBox1.setSelected(true);
                savedSearchCheckBox1.setSelected(true);
                publicationsOfflineCheckBox1.setSelected(true);
                normativeControlCheckBox1.setSelected(true);
                NDSubscribeCheckBox1.setSelected(true);
                consideredCopiesCheckBox1.setSelected(true);
                catalogueExportCheckBox1.setSelected(true);
                bookTabsCheckBox1.setSelected(true);
            }
        });
        CQMSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                asConfName = "CQMS";

                //Clear all checkboxes
                boxCleaner(serverPluginsPanel, false);
                boxCleaner(clientPluginsPanel, false);

                //Server clients conf
                bookTabsCheckBox.setSelected(true);
                catalogueExportCheckBox.setSelected(true);
                commentsCheckBox.setSelected(true);
                consideredCopiesCheckBox.setSelected(true);
                cqmsAuditsServerCheckBox.setSelected(true);
                cqmsFixationBaseCrmCheckBox.setSelected(true);
                cqmsFixationBaseServerCheckBox.setSelected(true);
                cqmsMonitoringServerCheckBox.setSelected(true);
                cqmsRisksServerCheckBox.setSelected(true);
                customConsideredCopiesReportCheckBox.setSelected(true);
                customNdChangeReportCheckBox.setSelected(true);
                customSubscriptionsReportServerCheckBox.setSelected(true);
                duplicatesCheckBox.setSelected(true);
                ebsConfigurationCheckBox.setSelected(true);
                expiringReportServerCheckBox.setSelected(true);
                favoritesCheckBox.setSelected(true);
                fastInputCheckBox.setSelected(true);
                fillReportServerCheckBox.setSelected(true);
                fundReportServerCheckBox.setSelected(true);
                //linkedReportServerCheckBox.setSelected(true);
                ndChangesServerCheckBox.setSelected(true);
                NDSubscribeCheckBox.setSelected(true);
                normativeControlCheckBox.setSelected(true);
                publicationsOfflineCheckBox.setSelected(true);
                registrationServerCheckBox.setSelected(true);
                reportsServerCheckBox.setSelected(true);
                savedSearchCheckBox.setSelected(true);
                usageReportServerCheckBox.setSelected(true);
                userDataCheckBox.setSelected(true);
                viewerCopiesCheckBox.setSelected(true);
                viewerLinkCheckBox.setSelected(true);
                workflowCheckBox.setSelected(true);
                xmlExportServerCheckBox.setSelected(true);

                //Client plugins conf
                bookTabsCheckBox1.setSelected(true);
                catalogueExportCheckBox1.setSelected(true);
                consideredCopiesCheckBox1.setSelected(true);
                duplicatesCheckBox1.setSelected(true);
                fastInputCheckBox1.setSelected(true);
                massInheritanceCheckBox.setSelected(true);
                NDSubscribeCheckBox1.setSelected(true);
                normativeControlCheckBox1.setSelected(true);
                publicationsOfflineCheckBox1.setSelected(true);
                registrationClientCheckBox.setSelected(true);
                savedSearchCheckBox1.setSelected(true);
                workflowCheckBox1.setSelected(true);
            }
        });
        KBNTIButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                asConfName = "KBNTI";

                //Clear all checkboxes
                boxCleaner(serverPluginsPanel, false);
                boxCleaner(clientPluginsPanel, false);

                //Server plugins conf
                actualizationCheckBox.setSelected(true);
                bookTabsCheckBox.setSelected(true);
                catalogueExportCheckBox.setSelected(true);
                commentsCheckBox.setSelected(true);
                consideredCopiesCheckBox.setSelected(true);
                customConsideredCopiesReportCheckBox.setSelected(true);
                customNdChangeReportCheckBox.setSelected(true);
                customSubscriptionsReportServerCheckBox.setSelected(true);
                ebsConfigurationCheckBox.setSelected(true);
                expiringReportServerCheckBox.setSelected(true);
                fastInputCheckBox.setSelected(true);
                favoritesCheckBox.setSelected(true);
                fillReportServerCheckBox.setSelected(true);
                fundReportServerCheckBox.setSelected(true);
                NDSubscribeCheckBox.setSelected(true);
                normativeControlCheckBox.setSelected(true);
                normdocsConvertersCheckBox.setSelected(true);
                publicationsOfflineCheckBox.setSelected(true);
                registrationServerCheckBox.setSelected(true);
                reportsServerCheckBox.setSelected(true);
                savedSearchCheckBox.setSelected(true);
                usageReportServerCheckBox.setSelected(true);
                userDataCheckBox.setSelected(true);
                viewerCopiesCheckBox.setSelected(true);
                viewerLinkCheckBox.setSelected(true);
                workflowCheckBox.setSelected(true);

                //Client plugins conf
                actualizationCheckBox1.setSelected(true);
                bookTabsCheckBox1.setSelected(true);
                catalogueExportCheckBox1.setSelected(true);
                consideredCopiesCheckBox1.setSelected(true);
                fastInputCheckBox1.setSelected(true);
                massInheritanceCheckBox.setSelected(true);
                NDSubscribeCheckBox1.setSelected(true);
                normativeControlCheckBox1.setSelected(true);
                publicationsOfflineCheckBox1.setSelected(true);
                registrationClientCheckBox.setSelected(true);
                savedSearchCheckBox1.setSelected(true);
                workflowCheckBox1.setSelected(true);
            }
        });

        allPluginsButton.addActionListener(new CheckAll(serverPluginsPanel, true));
        allPluginsButton.addActionListener(new CheckAll(clientPluginsPanel, true));
        ;
        clearPluginsButton.addActionListener(new CheckAll(serverPluginsPanel, false));
        clearPluginsButton.addActionListener(new CheckAll(clientPluginsPanel, false));

        //template conf
        this.templates = new ButtonGroup();
        templates.add(defaultRadioButton);
        templates.add(EBSTouchRadioButton);
        templates.add(EBSRadioButton);
        templates.add(MCHSTouchRadioButton);
        templates.add(MCHSRadioButton);
        templates.add(russianHelicoptersRadioButton);
        templates.add(OMKRadioButton);
        templates.add(KBNTIRadioButton);
        templates.add(KSMKRadioButton);
        templates.add(normdocsCatalogRadioButton);
        templates.add(KBNTI2RadioButton);
        templates.add(KBNTINDRadioButton);

        //template -> web app name listener
        defaultRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stormWebApplicationRadioButton.setSelected(true);
            }
        });
        EBSTouchRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stormWebApplicationTouchRadioButton.setSelected(true);
            }
        });
        EBSRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stormWebApplicationRadioButton.setSelected(true);
            }
        });
        MCHSTouchRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stormWebApplicationTouchRadioButton.setSelected(true);
            }
        });
        MCHSRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stormWebApplicationRadioButton.setSelected(true);
            }
        });
        russianHelicoptersRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stormWebApplicationRadioButton.setSelected(true);
            }
        });
        OMKRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stormWebApplicationRadioButton.setSelected(true);
            }
        });
        KBNTIRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stormWebApplicationRadioButton.setSelected(true);
            }
        });
        KSMKRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                KSMKRadioButton1.setSelected(true);
            }
        });
        normdocsCatalogRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stormWebApplicationRadioButton.setSelected(true);
            }
        });
        KBNTI2RadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stormWebApplicationRadioButton.setSelected(true);
            }
        });
        KBNTINDRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                KBNTINDRadioButton1.setSelected(true);
            }
        });

        //Web app name
        this.webAppName = new ButtonGroup();
        webAppName.add(stormWebApplicationRadioButton);
        webAppName.add(stormWebApplicationTouchRadioButton);
        webAppName.add(KSMKRadioButton1);
        webAppName.add(KBNTINDRadioButton1);

        //host name
        hostName = new ButtonGroup();
        hostName.add(testvmRadioButton);
        hostName.add(testvm3RadioButton);
        hostName.add(testvm6RadioButton);
        hostName.add(testvm7RadioButton);
        hostName.add(testvm10RadioButton);
        hostName.add(testvm11RadioButton);
        hostName.add(testvm12RadioButton);
        hostName.add(testvm13RadioButton);
        hostName.add(testvm14RadioButton);

        //DeployButtons
        deployWebApplicationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                InfoFromTables infoFromTables = getInfo();
                //tabbedPane1.setSelectedIndex(3);
                WebWorker webWorker = new WebWorker(infoFromTables, textArea1);
                webWorker.execute();
            }
        });
        deployArhiveServerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                InfoFromTables infoFromTables = getInfo();
                //здесьМногоИнфыПроTextArea.setText("Configuration: " + asConfName + "\nHost:" + infoFromTables.hostname + "\nServer plugins: " + infoFromTables.serverPlugins + "\nClient plugins: " + infoFromTables.clientPlugins);
                logger.info("\n----------------PluginList-------------------\n\tServer: \n" + infoFromTables.serverPlugins);
                logger.info("\n\tClient: \n" + infoFromTables.clientPlugins);
                //tabbedPane1.setSelectedIndex(3);
                ASWorker ASWorker = new ASWorker(infoFromTables, textArea1);
                ASWorker.execute();
            }
        });

        //CleanButton
        cleanTempButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    new fileUtil().deleteDirectory(new File(System.getProperty("user.home") + "\\vmUpdater\\"));
                } catch (Exception ignored) {
                }
            }
        });
    }

    private InfoFromTables getInfo() {
        InfoFromTables temp = new InfoFromTables();
        temp.hostname = getSelectedButtonText(hostName);
        temp.webTemplateName = getSelectedButtonText(templates);
        temp.tempateName = getSelectedButtonText(templates);
        temp.webAppName = getSelectedButtonText(webAppName);
        temp.tempateName = asConfName;
        addplugins(temp);
        return temp;
    }

    private void addplugins(InfoFromTables infoFromTables) {
        StringBuilder server = new StringBuilder(" ");
        if (actualizationCheckBox.isSelected())
            server.append("Actualization.jar\n");
        if (archiveDocsServerCheckBox.isSelected())
            server.append("archive-docs-server.jar\n");
        if (bookTabsCheckBox.isSelected())
            server.append("BookTabs.jar\n");
        if (catalogueExportCheckBox.isSelected())
            server.append("CatalogueExport.jar\n");
        if (commentsCheckBox.isSelected())
            server.append("Comments.jar\n");
        if (consideredCopiesCheckBox.isSelected())
            server.append("ConsideredCopies.jar\n");
        if (cqmsAuditsServerCheckBox.isSelected())
            server.append("cqms-audits-server.jar\n");
        if (cqmsFixationBaseCrmCheckBox.isSelected())
            server.append("cqms-fixation-base-crm-server.jar\n");
        if (cqmsFixationBaseServerCheckBox.isSelected())
            server.append("cqms-fixation-base-server.jar\n");
        if (cqmsMonitoringServerCheckBox.isSelected())
            server.append("cqms-monitoring-server.jar\n");
        if (customConsideredCopiesReportCheckBox.isSelected())
            server.append("custom-considered-copies-report-server.jar\n");
        if (customNdChangeReportCheckBox.isSelected())
            server.append("custom-nd-change-report-server.jar\n");
        if (customSubscriptionsReportServerCheckBox.isSelected())
            server.append("custom-subscriptions-report-server.jar\n");
        if (DBMigrationCheckBox.isSelected())
            server.append("DBMigration.jar\n");
        if (DIPImportCheckBox.isSelected())
            server.append("DIPImport.jar\n");
        if (documentReplacerCheckBox.isSelected())
            server.append("DocumentReplacer.jar\n");
        if (documentReplacerServerCheckBox.isSelected())
            server.append("document-replacer-server.jar\n");
        if (duplicatesCheckBox.isSelected())
            server.append("Duplicates.jar\n");
        if (ebsConfigurationCheckBox.isSelected())
            server.append("EbsConfiguration.jar\n");
        if (expiringReportServerCheckBox.isSelected())
            server.append("expiring-report-server.jar\n");
        if (fastInputCheckBox.isSelected())
            server.append("FastInput.jar\n");
        if (favoritesCheckBox.isSelected())
            server.append("Favorites.jar\n");
        if (FIASImportCheckBox.isSelected())
            server.append("FIASImport.jar\n");
        if (fillReportServerCheckBox.isSelected())
            server.append("fill-report-server.jar\n");
        if (fullPreviewFilesCheckBox.isSelected())
            server.append("FullPreviewFiles.jar\n");
        if (fundReportServerCheckBox.isSelected())
            server.append("fund-report-server.jar\n");
        if (helpPluginCheckBox.isSelected())
            server.append("HelpPlugin.jar\n");
        if (kbntindExtensionServerCheckBox.isSelected())
            server.append("kbntind-extension-server.jar\n");
        if (linkedReportServerCheckBox.isSelected())
            server.append("linked-report-server.jar\n");
        if (ndChangesServerCheckBox.isSelected())
            server.append("nd-changes-server.jar\n");
        if (NDSubscribeCheckBox.isSelected())
            server.append("ND-subscribe.jar\n");
        if (normativeControlCheckBox.isSelected())
            server.append("NormativeControl.jar\n");
        if (normdocsConvertersCheckBox.isSelected())
            server.append("NormdocsConverters.jar\n");
        if (omkExtensionServerCheckBox.isSelected())
            server.append("omk-extension-server.jar\n");
        if (omkWssServerCheckBox.isSelected())
            server.append("omk-wss-server.jar\n");
        if (publicationsOfflineCheckBox.isSelected())
            server.append("PublicationsOffline.jar\n");
        if (publicFileAccessCheckBox.isSelected())
            server.append("PublicFileAccess.jar\n");
        if (ratingPluginCheckBox.isSelected())
            server.append("RatingPlugin.jar\n");
        if (registrationServerCheckBox.isSelected())
            server.append("registration-server.jar\n");
        if (reportsServerCheckBox.isSelected())
            server.append("reports-server.jar\n");
        if (reviewsCheckBox.isSelected())
            server.append("Reviews.jar\n");
        if (RSSPluginCheckBox.isSelected())
            server.append("RSSPlugin.jar\n");
        if (savedSearchCheckBox.isSelected())
            server.append("SavedSearch.jar\n");
        if (selectionsCheckBox.isSelected())
            server.append("Selections.jar\n");
        if (stubfileServerCheckBox.isSelected())
            server.append("stubfile-server.jar\n");
        if (synchronizationServerCheckBox.isSelected()) {
            infoFromTables.synchroProxy = true;
            server.append("synchronization-server.jar\n");
        }
        if (testSystemCheckBox.isSelected())
            server.append("TestSystem.jar\n");
        if (usageReportServerCheckBox.isSelected())
            server.append("usage-report-server.jar\n");
        if (userDataCheckBox.isSelected())
            server.append("UserData.jar\n");
        if (viewerCopiesCheckBox.isSelected())
            server.append("ViewerCopies.jar\n");
        if (viewerLinkCheckBox.isSelected())
            server.append("ViewerLink.jar\n");
        if (votingCheckBox.isSelected())
            server.append("Voting.jar\n");
        if (vtb24pluginServerCheckBox.isSelected())
            server.append("vtb24plugin-server.jar\n");
        if (workflowCheckBox.isSelected())
            server.append("Workflow.jar\n");
        if (xmlExportServerCheckBox.isSelected())
            server.append("xml-export-server.jar\n");
        if (cqmsRisksServerCheckBox.isSelected())
            server.append("cqms-risk-server.jar\n");
        if (backupSpotCheckBoxServer.isSelected())
            server.append("BackupSpot.jar\n");
        infoFromTables.serverPlugins = server.toString();

        StringBuilder client = new StringBuilder();
        if (bookTabsCheckBox1.isSelected())
            client.append("BookTabs.jar\n");
        if (actualizationCheckBox1.isSelected())
            client.append("Actualization.jar\n");
        if (archiveDocsClientCheckBox.isSelected())
            client.append("archive-docs-client.jar\n");
        if (avExcelImportClientCheckBox.isSelected())
            client.append("av-excel-import-client.jar\n");
        if (backupSpotCheckBox.isSelected())
            client.append("BackupSpot.jar\n");
        if (catalogueExportCheckBox1.isSelected())
            client.append("CatalogueExport.jar\n");
        if (consideredCopiesCheckBox1.isSelected())
            client.append("ConsideredCopies.jar\n");
        if (DIPImportCheckBox1.isSelected())
            client.append("DIPImport.jar\n");
        if (duplicatesCheckBox1.isSelected())
            client.append("Duplicates.jar\n");
        if (fastInputCheckBox1.isSelected())
            client.append("FastInput.jar\n");
        if (foldersHierarchyCheckBox.isSelected())
            client.append("FoldersHierarchy.jar\n");
        if (helpPluginCheckBox1.isSelected())
            client.append("HelpPlugin.jar\n");
        if (massInheritanceCheckBox.isSelected())
            client.append("MassInheritance.jar\n");
        if (MGOKCheckBox.isSelected())
            client.append("MGOK.jar\n");
        if (NDSubscribeCheckBox1.isSelected())
            client.append("ND-subscribe.jar\n");
        if (normativeControlCheckBox1.isSelected())
            client.append("NormativeControl.jar\n");
        if (normdocsConvertersCheckBox1.isSelected())
            client.append("NormdocsConverters.jar\n");
        if (publicationsOfflineCheckBox1.isSelected())
            client.append("PublicationsOffline.jar\n");
        if (ratingPluginCheckBox1.isSelected())
            client.append("RatingPlugin.jar\n");
        if (registrationClientCheckBox.isSelected())
            client.append("registration-client.jar\n");
        if (RSSPluginCheckBox1.isSelected())
            client.append("RSSPlugin.jar\n");
        if (savedSearchCheckBox1.isSelected())
            client.append("SavedSearch.jar\n");
        if (selectionsCheckBox1.isSelected())
            client.append("Selections.jar\n");
        if (stubfileClientCheckBox.isSelected())
            client.append("stubfile-client.jar\n");
        if (testSystemCheckBox1.isSelected())
            client.append("TestSystem.jar\n");
        if (vtb24pluginClientCheckBox.isSelected())
            client.append("vtb24plugin-client.jar\n");
        if (workflowCheckBox1.isSelected())
            client.append("Workflow.jar\n");
        infoFromTables.clientPlugins = client.toString();
    }

    private void redirectCOUT() {
        //JScrollPane.
        /*
        PrintStream printStream = new PrintStream(new CustomOutputStream(textArea1));
        standardOut = System.out;
        System.setOut(printStream);
        System.setErr(printStream);
        */
        /*
        System.setOut(new PrintStreamCapturer(textArea1, System.out));
        System.setErr(new PrintStreamCapturer(textArea1, System.err, "[ERROR] "));
        logger.info("Log started here");*/
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel1.setBorder(BorderFactory.createTitledBorder("Lazy vm updater"));
        tabbedPane1 = new JTabbedPane();
        panel1.add(tabbedPane1, new GridConstraints(0, 0, 2, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(200, 200), null, 0, false));
        webAppPanel = new JPanel();
        webAppPanel.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Web Application", webAppPanel);
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new GridBagLayout());
        webAppPanel.add(panel4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel4.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(25, 5, 5, 5), "Select template"));
        defaultRadioButton = new JRadioButton();
        defaultRadioButton.setSelected(true);
        defaultRadioButton.setText("Default");
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel4.add(defaultRadioButton, gbc);
        EBSTouchRadioButton = new JRadioButton();
        EBSTouchRadioButton.setText("EBSTouch");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel4.add(EBSTouchRadioButton, gbc);
        EBSRadioButton = new JRadioButton();
        EBSRadioButton.setText("EBS");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel4.add(EBSRadioButton, gbc);
        MCHSTouchRadioButton = new JRadioButton();
        MCHSTouchRadioButton.setText("MCHSTouch");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.weightx = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel4.add(MCHSTouchRadioButton, gbc);
        MCHSRadioButton = new JRadioButton();
        MCHSRadioButton.setText("MCHS");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.weightx = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel4.add(MCHSRadioButton, gbc);
        russianHelicoptersRadioButton = new JRadioButton();
        russianHelicoptersRadioButton.setText("RussianHelicopters");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.weightx = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel4.add(russianHelicoptersRadioButton, gbc);
        OMKRadioButton = new JRadioButton();
        OMKRadioButton.setText("OMK");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.weightx = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel4.add(OMKRadioButton, gbc);
        KBNTIRadioButton = new JRadioButton();
        KBNTIRadioButton.setText("KBNTI");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.weightx = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel4.add(KBNTIRadioButton, gbc);
        KSMKRadioButton = new JRadioButton();
        KSMKRadioButton.setText("KSMK");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.weightx = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel4.add(KSMKRadioButton, gbc);
        normdocsCatalogRadioButton = new JRadioButton();
        normdocsCatalogRadioButton.setText("NormdocsCatalog");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 9;
        gbc.weightx = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel4.add(normdocsCatalogRadioButton, gbc);
        KBNTI2RadioButton = new JRadioButton();
        KBNTI2RadioButton.setText("KBNTI2");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 10;
        gbc.weightx = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel4.add(KBNTI2RadioButton, gbc);
        KBNTINDRadioButton = new JRadioButton();
        KBNTINDRadioButton.setText("KBNTIND");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 11;
        gbc.weightx = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel4.add(KBNTINDRadioButton, gbc);
        final JLabel label1 = new JLabel();
        label1.setText("");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 12;
        gbc.weighty = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel4.add(label1, gbc);
        final JPanel panel5 = new JPanel();
        panel5.setLayout(new GridBagLayout());
        webAppPanel.add(panel5, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel5.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(25, 5, 5, 5), "Select application name"));
        stormWebApplicationRadioButton = new JRadioButton();
        stormWebApplicationRadioButton.setSelected(true);
        stormWebApplicationRadioButton.setText("StormWebApplication");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel5.add(stormWebApplicationRadioButton, gbc);
        stormWebApplicationTouchRadioButton = new JRadioButton();
        stormWebApplicationTouchRadioButton.setText("StormWebApplicationTouch");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel5.add(stormWebApplicationTouchRadioButton, gbc);
        KSMKRadioButton1 = new JRadioButton();
        KSMKRadioButton1.setText("KSMK");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel5.add(KSMKRadioButton1, gbc);
        KBNTINDRadioButton1 = new JRadioButton();
        KBNTINDRadioButton1.setText("KBNTIND");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 2, 2, 2);
        panel5.add(KBNTINDRadioButton1, gbc);
        final JLabel label2 = new JLabel();
        label2.setText("");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.weightx = 100.0;
        gbc.weighty = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel5.add(label2, gbc);
        arhiveServerPanel = new JPanel();
        arhiveServerPanel.setLayout(new GridLayoutManager(1, 3, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Archive Server", arhiveServerPanel);
        final JPanel panel6 = new JPanel();
        panel6.setLayout(new GridBagLayout());
        arhiveServerPanel.add(panel6, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel6.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(5, 5, 5, 0), "Configurations"));
        defaultButton = new JButton();
        defaultButton.setAutoscrolls(false);
        defaultButton.setBorderPainted(false);
        defaultButton.setEnabled(true);
        defaultButton.setFocusCycleRoot(true);
        defaultButton.setHorizontalAlignment(0);
        defaultButton.setSelected(false);
        defaultButton.setText("Default");
        defaultButton.setToolTipText("Доступно для нажатия");
        defaultButton.setVerticalAlignment(0);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 0, 0, 0);
        panel6.add(defaultButton, gbc);
        ebsButton = new JButton();
        ebsButton.setAutoscrolls(false);
        ebsButton.setBorderPainted(false);
        ebsButton.setSelected(false);
        ebsButton.setText("Ebs");
        ebsButton.setToolTipText("Доступно для нажатия");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 0, 0, 0);
        panel6.add(ebsButton, gbc);
        mchsButton = new JButton();
        mchsButton.setAutoscrolls(false);
        mchsButton.setBorderPainted(false);
        mchsButton.setSelected(false);
        mchsButton.setText("Mchs");
        mchsButton.setToolTipText("Доступно для нажатия");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 0, 0, 0);
        panel6.add(mchsButton, gbc);
        helicoptersButton = new JButton();
        helicoptersButton.setAutoscrolls(false);
        helicoptersButton.setBorderPainted(false);
        helicoptersButton.setSelected(false);
        helicoptersButton.setText("Helicopters");
        helicoptersButton.setToolTipText("Доступно для нажатия");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 0, 0, 0);
        panel6.add(helicoptersButton, gbc);
        OMKButton = new JButton();
        OMKButton.setAutoscrolls(false);
        OMKButton.setBorderPainted(false);
        OMKButton.setSelected(false);
        OMKButton.setText("OMK");
        OMKButton.setToolTipText("Доступно для нажатия");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 0, 0, 0);
        panel6.add(OMKButton, gbc);
        CQMSButton = new JButton();
        CQMSButton.setAutoscrolls(false);
        CQMSButton.setBorderPainted(false);
        CQMSButton.setSelected(false);
        CQMSButton.setText("CQMS");
        CQMSButton.setToolTipText("Доступно для нажатия");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 0, 0, 0);
        panel6.add(CQMSButton, gbc);
        KBNTIButton = new JButton();
        KBNTIButton.setAutoscrolls(false);
        KBNTIButton.setBorderPainted(false);
        KBNTIButton.setSelected(false);
        KBNTIButton.setText("KBNTI");
        KBNTIButton.setToolTipText("Доступно для нажатия");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 0, 0, 0);
        panel6.add(KBNTIButton, gbc);
        allPluginsButton = new JButton();
        allPluginsButton.setAutoscrolls(false);
        allPluginsButton.setBorderPainted(false);
        allPluginsButton.setSelected(false);
        allPluginsButton.setText("All plugins");
        allPluginsButton.setToolTipText("Доступно для нажатия");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 0, 0, 0);
        panel6.add(allPluginsButton, gbc);
        clearPluginsButton = new JButton();
        clearPluginsButton.setAutoscrolls(false);
        clearPluginsButton.setBorderPainted(false);
        clearPluginsButton.setSelected(false);
        clearPluginsButton.setText("Clear plugins");
        clearPluginsButton.setToolTipText("Доступно для нажатия");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 0, 0, 0);
        panel6.add(clearPluginsButton, gbc);
        final JLabel label3 = new JLabel();
        label3.setEnabled(true);
        label3.setText("");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 9;
        gbc.weightx = 100.0;
        gbc.weighty = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel6.add(label3, gbc);
        serverPluginsPanel = new JPanel();
        serverPluginsPanel.setLayout(new GridBagLayout());
        arhiveServerPanel.add(serverPluginsPanel, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        serverPluginsPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Server plugins"));
        actualizationCheckBox = new JCheckBox();
        actualizationCheckBox.setText("Actualization");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(actualizationCheckBox, gbc);
        archiveDocsServerCheckBox = new JCheckBox();
        archiveDocsServerCheckBox.setText("archive-docs-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(archiveDocsServerCheckBox, gbc);
        bookTabsCheckBox = new JCheckBox();
        bookTabsCheckBox.setText("BookTabs");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(bookTabsCheckBox, gbc);
        catalogueExportCheckBox = new JCheckBox();
        catalogueExportCheckBox.setText("CatalogueExport");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(catalogueExportCheckBox, gbc);
        commentsCheckBox = new JCheckBox();
        commentsCheckBox.setText("Comments");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(commentsCheckBox, gbc);
        consideredCopiesCheckBox = new JCheckBox();
        consideredCopiesCheckBox.setText("ConsideredCopies");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(consideredCopiesCheckBox, gbc);
        cqmsAuditsServerCheckBox = new JCheckBox();
        cqmsAuditsServerCheckBox.setText("cqms-audits-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(cqmsAuditsServerCheckBox, gbc);
        cqmsFixationBaseCrmCheckBox = new JCheckBox();
        cqmsFixationBaseCrmCheckBox.setText("cqms-fixation-base-crm-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(cqmsFixationBaseCrmCheckBox, gbc);
        cqmsFixationBaseServerCheckBox = new JCheckBox();
        cqmsFixationBaseServerCheckBox.setText("cqms-fixation-base-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(cqmsFixationBaseServerCheckBox, gbc);
        cqmsMonitoringServerCheckBox = new JCheckBox();
        cqmsMonitoringServerCheckBox.setText("cqms-monitoring-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 9;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(cqmsMonitoringServerCheckBox, gbc);
        customConsideredCopiesReportCheckBox = new JCheckBox();
        customConsideredCopiesReportCheckBox.setText("custom-considered-copies-report-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 11;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(customConsideredCopiesReportCheckBox, gbc);
        customNdChangeReportCheckBox = new JCheckBox();
        customNdChangeReportCheckBox.setText("custom-nd-change-report-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 12;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(customNdChangeReportCheckBox, gbc);
        customSubscriptionsReportServerCheckBox = new JCheckBox();
        customSubscriptionsReportServerCheckBox.setText("custom-subscriptions-report-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 13;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(customSubscriptionsReportServerCheckBox, gbc);
        DBMigrationCheckBox = new JCheckBox();
        DBMigrationCheckBox.setText("DBMigration");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 14;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(DBMigrationCheckBox, gbc);
        DIPImportCheckBox = new JCheckBox();
        DIPImportCheckBox.setText("DIPImport");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 15;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(DIPImportCheckBox, gbc);
        documentReplacerCheckBox = new JCheckBox();
        documentReplacerCheckBox.setText("DocumentReplacer");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 16;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(documentReplacerCheckBox, gbc);
        documentReplacerServerCheckBox = new JCheckBox();
        documentReplacerServerCheckBox.setText("document-replacer-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 17;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(documentReplacerServerCheckBox, gbc);
        duplicatesCheckBox = new JCheckBox();
        duplicatesCheckBox.setText("Duplicates");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 18;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(duplicatesCheckBox, gbc);
        ebsConfigurationCheckBox = new JCheckBox();
        ebsConfigurationCheckBox.setText("EbsConfiguration");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 19;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(ebsConfigurationCheckBox, gbc);
        expiringReportServerCheckBox = new JCheckBox();
        expiringReportServerCheckBox.setText("expiring-report-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 20;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(expiringReportServerCheckBox, gbc);
        fastInputCheckBox = new JCheckBox();
        fastInputCheckBox.setText("FastInput");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 21;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(fastInputCheckBox, gbc);
        favoritesCheckBox = new JCheckBox();
        favoritesCheckBox.setText("Favorites");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 22;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(favoritesCheckBox, gbc);
        FIASImportCheckBox = new JCheckBox();
        FIASImportCheckBox.setText("FIASImport");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 23;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(FIASImportCheckBox, gbc);
        fillReportServerCheckBox = new JCheckBox();
        fillReportServerCheckBox.setText("fill-report-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 24;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(fillReportServerCheckBox, gbc);
        fullPreviewFilesCheckBox = new JCheckBox();
        fullPreviewFilesCheckBox.setText("FullPreviewFiles");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 25;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(fullPreviewFilesCheckBox, gbc);
        fundReportServerCheckBox = new JCheckBox();
        fundReportServerCheckBox.setText("fund-report-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 26;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(fundReportServerCheckBox, gbc);
        helpPluginCheckBox = new JCheckBox();
        helpPluginCheckBox.setText("HelpPlugin");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 27;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(helpPluginCheckBox, gbc);
        kbntindExtensionServerCheckBox = new JCheckBox();
        kbntindExtensionServerCheckBox.setText("kbntind-extension-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 28;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(kbntindExtensionServerCheckBox, gbc);
        linkedReportServerCheckBox = new JCheckBox();
        linkedReportServerCheckBox.setText("linked-report-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 29;
        gbc.weightx = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(linkedReportServerCheckBox, gbc);
        ndChangesServerCheckBox = new JCheckBox();
        ndChangesServerCheckBox.setText("nd-changes-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(ndChangesServerCheckBox, gbc);
        NDSubscribeCheckBox = new JCheckBox();
        NDSubscribeCheckBox.setText("ND-subscribe");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(NDSubscribeCheckBox, gbc);
        normativeControlCheckBox = new JCheckBox();
        normativeControlCheckBox.setText("NormativeControl");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(normativeControlCheckBox, gbc);
        normdocsConvertersCheckBox = new JCheckBox();
        normdocsConvertersCheckBox.setText("NormdocsConverters");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(normdocsConvertersCheckBox, gbc);
        omkExtensionServerCheckBox = new JCheckBox();
        omkExtensionServerCheckBox.setText("omk-extension-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(omkExtensionServerCheckBox, gbc);
        omkWssServerCheckBox = new JCheckBox();
        omkWssServerCheckBox.setText("omk-wss-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(omkWssServerCheckBox, gbc);
        publicationsOfflineCheckBox = new JCheckBox();
        publicationsOfflineCheckBox.setText("PublicationsOffline");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 6;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(publicationsOfflineCheckBox, gbc);
        publicFileAccessCheckBox = new JCheckBox();
        publicFileAccessCheckBox.setText("PublicFileAccess");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 7;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(publicFileAccessCheckBox, gbc);
        ratingPluginCheckBox = new JCheckBox();
        ratingPluginCheckBox.setText("RatingPlugin");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 8;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(ratingPluginCheckBox, gbc);
        registrationServerCheckBox = new JCheckBox();
        registrationServerCheckBox.setText("registration-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 9;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(registrationServerCheckBox, gbc);
        final JLabel label4 = new JLabel();
        label4.setText("");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 30;
        gbc.weightx = 100.0;
        gbc.weighty = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(label4, gbc);
        cqmsRisksServerCheckBox = new JCheckBox();
        cqmsRisksServerCheckBox.setText("cqms-risks-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 10;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(cqmsRisksServerCheckBox, gbc);
        reportsServerCheckBox = new JCheckBox();
        reportsServerCheckBox.setText("reports-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 10;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(reportsServerCheckBox, gbc);
        reviewsCheckBox = new JCheckBox();
        reviewsCheckBox.setText("Reviews");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 11;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(reviewsCheckBox, gbc);
        RSSPluginCheckBox = new JCheckBox();
        RSSPluginCheckBox.setText("RSSPlugin");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 12;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(RSSPluginCheckBox, gbc);
        savedSearchCheckBox = new JCheckBox();
        savedSearchCheckBox.setText("SavedSearch");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 13;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(savedSearchCheckBox, gbc);
        selectionsCheckBox = new JCheckBox();
        selectionsCheckBox.setText("Selections");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 14;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(selectionsCheckBox, gbc);
        stubfileServerCheckBox = new JCheckBox();
        stubfileServerCheckBox.setText("stubfile-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 15;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(stubfileServerCheckBox, gbc);
        synchronizationServerCheckBox = new JCheckBox();
        synchronizationServerCheckBox.setText("synchronization-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 16;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(synchronizationServerCheckBox, gbc);
        testSystemCheckBox = new JCheckBox();
        testSystemCheckBox.setText("TestSystem");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 17;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(testSystemCheckBox, gbc);
        usageReportServerCheckBox = new JCheckBox();
        usageReportServerCheckBox.setText("usage-report-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 18;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(usageReportServerCheckBox, gbc);
        userDataCheckBox = new JCheckBox();
        userDataCheckBox.setText("UserData");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 19;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(userDataCheckBox, gbc);
        viewerCopiesCheckBox = new JCheckBox();
        viewerCopiesCheckBox.setText("ViewerCopies");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 20;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(viewerCopiesCheckBox, gbc);
        viewerLinkCheckBox = new JCheckBox();
        viewerLinkCheckBox.setText("ViewerLink");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 21;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(viewerLinkCheckBox, gbc);
        votingCheckBox = new JCheckBox();
        votingCheckBox.setText("Voting");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 22;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(votingCheckBox, gbc);
        vtb24pluginServerCheckBox = new JCheckBox();
        vtb24pluginServerCheckBox.setText("vtb24plugin-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 23;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(vtb24pluginServerCheckBox, gbc);
        workflowCheckBox = new JCheckBox();
        workflowCheckBox.setText("Workflow");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 24;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(workflowCheckBox, gbc);
        xmlExportServerCheckBox = new JCheckBox();
        xmlExportServerCheckBox.setText("xml-export-server");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 25;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(xmlExportServerCheckBox, gbc);
        backupSpotCheckBoxServer = new JCheckBox();
        backupSpotCheckBoxServer.setText("BackupSpot");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 26;
        gbc.anchor = GridBagConstraints.WEST;
        serverPluginsPanel.add(backupSpotCheckBoxServer, gbc);
        clientPluginsPanel = new JPanel();
        clientPluginsPanel.setLayout(new GridBagLayout());
        arhiveServerPanel.add(clientPluginsPanel, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        clientPluginsPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Client plugins"));
        actualizationCheckBox1 = new JCheckBox();
        actualizationCheckBox1.setText("Actualization");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(actualizationCheckBox1, gbc);
        archiveDocsClientCheckBox = new JCheckBox();
        archiveDocsClientCheckBox.setText("archive-docs-client");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(archiveDocsClientCheckBox, gbc);
        avExcelImportClientCheckBox = new JCheckBox();
        avExcelImportClientCheckBox.setText("av-excel-import-client");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(avExcelImportClientCheckBox, gbc);
        backupSpotCheckBox = new JCheckBox();
        backupSpotCheckBox.setText("BackupSpot");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(backupSpotCheckBox, gbc);
        catalogueExportCheckBox1 = new JCheckBox();
        catalogueExportCheckBox1.setText("CatalogueExport");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(catalogueExportCheckBox1, gbc);
        consideredCopiesCheckBox1 = new JCheckBox();
        consideredCopiesCheckBox1.setText("ConsideredCopies");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(consideredCopiesCheckBox1, gbc);
        DIPImportCheckBox1 = new JCheckBox();
        DIPImportCheckBox1.setText("DIPImport");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(DIPImportCheckBox1, gbc);
        duplicatesCheckBox1 = new JCheckBox();
        duplicatesCheckBox1.setText("Duplicates");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(duplicatesCheckBox1, gbc);
        fastInputCheckBox1 = new JCheckBox();
        fastInputCheckBox1.setText("FastInput");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 9;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(fastInputCheckBox1, gbc);
        foldersHierarchyCheckBox = new JCheckBox();
        foldersHierarchyCheckBox.setText("FoldersHierarchy");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 10;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(foldersHierarchyCheckBox, gbc);
        helpPluginCheckBox1 = new JCheckBox();
        helpPluginCheckBox1.setText("HelpPlugin");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 11;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(helpPluginCheckBox1, gbc);
        massInheritanceCheckBox = new JCheckBox();
        massInheritanceCheckBox.setText("MassInheritance");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 12;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(massInheritanceCheckBox, gbc);
        MGOKCheckBox = new JCheckBox();
        MGOKCheckBox.setText("MGOK");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 13;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(MGOKCheckBox, gbc);
        NDSubscribeCheckBox1 = new JCheckBox();
        NDSubscribeCheckBox1.setText("ND-subscribe");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 14;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(NDSubscribeCheckBox1, gbc);
        normativeControlCheckBox1 = new JCheckBox();
        normativeControlCheckBox1.setText("NormativeControl");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 15;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(normativeControlCheckBox1, gbc);
        publicationsOfflineCheckBox1 = new JCheckBox();
        publicationsOfflineCheckBox1.setText("PublicationsOffline");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 17;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(publicationsOfflineCheckBox1, gbc);
        ratingPluginCheckBox1 = new JCheckBox();
        ratingPluginCheckBox1.setText("RatingPlugin");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 18;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(ratingPluginCheckBox1, gbc);
        registrationClientCheckBox = new JCheckBox();
        registrationClientCheckBox.setText("registration-client");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 19;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(registrationClientCheckBox, gbc);
        RSSPluginCheckBox1 = new JCheckBox();
        RSSPluginCheckBox1.setText("RSSPlugin");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 20;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(RSSPluginCheckBox1, gbc);
        savedSearchCheckBox1 = new JCheckBox();
        savedSearchCheckBox1.setText("SavedSearch");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 21;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(savedSearchCheckBox1, gbc);
        selectionsCheckBox1 = new JCheckBox();
        selectionsCheckBox1.setText("Selections");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 22;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(selectionsCheckBox1, gbc);
        stubfileClientCheckBox = new JCheckBox();
        stubfileClientCheckBox.setText("stubfile-client");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 23;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(stubfileClientCheckBox, gbc);
        testSystemCheckBox1 = new JCheckBox();
        testSystemCheckBox1.setText("TestSystem");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 24;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(testSystemCheckBox1, gbc);
        vtb24pluginClientCheckBox = new JCheckBox();
        vtb24pluginClientCheckBox.setText("vtb24plugin-client");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 25;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(vtb24pluginClientCheckBox, gbc);
        workflowCheckBox1 = new JCheckBox();
        workflowCheckBox1.setText("Workflow");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 26;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(workflowCheckBox1, gbc);
        final JLabel label5 = new JLabel();
        label5.setText("");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 27;
        gbc.weightx = 100.0;
        gbc.weighty = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(label5, gbc);
        bookTabsCheckBox1 = new JCheckBox();
        bookTabsCheckBox1.setText("BookTabs");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(bookTabsCheckBox1, gbc);
        normdocsConvertersCheckBox1 = new JCheckBox();
        normdocsConvertersCheckBox1.setText("NormdocsConverters");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 16;
        gbc.anchor = GridBagConstraints.WEST;
        clientPluginsPanel.add(normdocsConvertersCheckBox1, gbc);
        Deploy = new JPanel();
        Deploy.setLayout(new GridLayoutManager(4, 4, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Deploy", Deploy);
        panel2 = new JPanel();
        panel2.setLayout(new GridBagLayout());
        Deploy.add(panel2, new GridConstraints(0, 0, 1, 4, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Deploy"));
        deployWebApplicationButton = new JButton();
        deployWebApplicationButton.setText("Deploy Web Application");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 100.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 0, 0, 2);
        panel2.add(deployWebApplicationButton, gbc);
        final JLabel label6 = new JLabel();
        label6.setText("");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weighty = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel2.add(label6, gbc);
        deployArhiveServerButton = new JButton();
        deployArhiveServerButton.setText("Deploy Arhive Server");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 100.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 2, 0, 0);
        panel2.add(deployArhiveServerButton, gbc);
        webAppInfo = new JPanel();
        webAppInfo.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        Deploy.add(webAppInfo, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, true));
        webAppInfo.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Web app info"));
        final JScrollPane scrollPane1 = new JScrollPane();
        webAppInfo.add(scrollPane1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        здесьНемногоИнфыПроTextArea = new JTextArea();
        Font здесьНемногоИнфыПроTextAreaFont = this.$$$getFont$$$("Lucida Sans Typewriter", Font.PLAIN, 14, здесьНемногоИнфыПроTextArea.getFont());
        if (здесьНемногоИнфыПроTextAreaFont != null)
            здесьНемногоИнфыПроTextArea.setFont(здесьНемногоИнфыПроTextAreaFont);
        здесьНемногоИнфыПроTextArea.setText("");
        scrollPane1.setViewportView(здесьНемногоИнфыПроTextArea);
        arhiveServerInfo = new JPanel();
        arhiveServerInfo.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        Deploy.add(arhiveServerInfo, new GridConstraints(3, 1, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, true));
        arhiveServerInfo.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Arhive server info"));
        final JScrollPane scrollPane2 = new JScrollPane();
        scrollPane2.setHorizontalScrollBarPolicy(30);
        scrollPane2.setVerticalScrollBarPolicy(22);
        arhiveServerInfo.add(scrollPane2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        здесьМногоИнфыПроTextArea = new JTextArea();
        Font здесьМногоИнфыПроTextAreaFont = this.$$$getFont$$$("Lucida Sans Typewriter", Font.PLAIN, 14, здесьМногоИнфыПроTextArea.getFont());
        if (здесьМногоИнфыПроTextAreaFont != null) здесьМногоИнфыПроTextArea.setFont(здесьМногоИнфыПроTextAreaFont);
        здесьМногоИнфыПроTextArea.setText("");
        scrollPane2.setViewportView(здесьМногоИнфыПроTextArea);
        final JPanel panel7 = new JPanel();
        panel7.setLayout(new GridBagLayout());
        Deploy.add(panel7, new GridConstraints(1, 0, 2, 2, GridConstraints.ANCHOR_NORTH, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        panel7.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Test vm"));
        final JLabel label7 = new JLabel();
        label7.setText("");
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.weightx = 100.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel7.add(label7, gbc);
        testvmRadioButton = new JRadioButton();
        testvmRadioButton.setSelected(true);
        testvmRadioButton.setText("testvm");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel7.add(testvmRadioButton, gbc);
        testvm3RadioButton = new JRadioButton();
        testvm3RadioButton.setText("testvm3");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel7.add(testvm3RadioButton, gbc);
        testvm6RadioButton = new JRadioButton();
        testvm6RadioButton.setText("testvm6");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel7.add(testvm6RadioButton, gbc);
        testvm7RadioButton = new JRadioButton();
        testvm7RadioButton.setText("testvm7");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel7.add(testvm7RadioButton, gbc);
        testvm10RadioButton = new JRadioButton();
        testvm10RadioButton.setText("testvm10");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel7.add(testvm10RadioButton, gbc);
        testvm11RadioButton = new JRadioButton();
        testvm11RadioButton.setText("testvm11");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.gridwidth = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel7.add(testvm11RadioButton, gbc);
        testvm12RadioButton = new JRadioButton();
        testvm12RadioButton.setText("testvm12");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.gridwidth = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel7.add(testvm12RadioButton, gbc);
        testvm13RadioButton = new JRadioButton();
        testvm13RadioButton.setText("testvm13");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.gridwidth = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel7.add(testvm13RadioButton, gbc);
        testvm14RadioButton = new JRadioButton();
        testvm14RadioButton.setText("testvm14");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.gridwidth = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel7.add(testvm14RadioButton, gbc);
        final JLabel label8 = new JLabel();
        label8.setText("");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 9;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel7.add(label8, gbc);
        final JPanel panel8 = new JPanel();
        panel8.setLayout(new GridLayoutManager(6, 1, new Insets(0, 0, 0, 0), -1, -1));
        Deploy.add(panel8, new GridConstraints(1, 2, 2, 2, GridConstraints.ANCHOR_NORTHWEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        panel8.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Cmd"));
        stopButton = new JButton();
        stopButton.setText("Stop");
        panel8.add(stopButton, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        startButton = new JButton();
        startButton.setText("Start");
        panel8.add(startButton, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        restartButton = new JButton();
        restartButton.setText("Restart");
        panel8.add(restartButton, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        showLogButton = new JButton();
        showLogButton.setText("Show Log");
        panel8.add(showLogButton, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        stopLogButton = new JButton();
        stopLogButton.setText("Stop Log");
        panel8.add(stopLogButton, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        killJavaButton = new JButton();
        killJavaButton.setText("Kill Java");
        panel8.add(killJavaButton, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        logPanel1 = new JPanel();
        logPanel1.setLayout(new GridLayoutManager(2, 3, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Log", logPanel1);
        final JScrollPane scrollPane3 = new JScrollPane();
        scrollPane3.setAutoscrolls(true);
        scrollPane3.setFocusCycleRoot(true);
        scrollPane3.setFocusTraversalPolicyProvider(true);
        scrollPane3.setVerticalScrollBarPolicy(20);
        logPanel1.add(scrollPane3, new GridConstraints(0, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        textArea1 = new JTextArea();
        textArea1.setAutoscrolls(false);
        textArea1.setEditable(false);
        textArea1.setFocusCycleRoot(false);
        textArea1.setFocusable(true);
        Font textArea1Font = this.$$$getFont$$$("Droid Sans Mono Slashed", Font.PLAIN, 16, textArea1.getFont());
        if (textArea1Font != null) textArea1.setFont(textArea1Font);
        scrollPane3.setViewportView(textArea1);
        cleanButton = new JButton();
        cleanButton.setText("Clean");
        logPanel1.add(cleanButton, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        refreshButton = new JButton();
        refreshButton.setText("Refresh");
        logPanel1.add(refreshButton, new GridConstraints(1, 1, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        vmInfo = new JPanel();
        vmInfo.setLayout(new GridBagLayout());
        tabbedPane1.addTab("vm INFO", vmInfo);
        vmInfo.setBorder(BorderFactory.createTitledBorder(""));
        final JPanel panel9 = new JPanel();
        panel9.setLayout(new GridBagLayout());
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 100.0;
        gbc.weighty = 100.0;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(1, 1, 1, 1);
        vmInfo.add(panel9, gbc);
        panel9.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "vm Status"));
        final JLabel label9 = new JLabel();
        label9.setText("testvm :");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(label9, gbc);
        testvmStatus = new JLabel();
        testvmStatus.setText("Label");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 0;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(testvmStatus, gbc);
        final JLabel label10 = new JLabel();
        label10.setText("testvm3 :");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(label10, gbc);
        testvm3Status = new JLabel();
        testvm3Status.setText("Label");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 1;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(testvm3Status, gbc);
        final JLabel label11 = new JLabel();
        label11.setText("testvm6 :");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(label11, gbc);
        testvm6Status = new JLabel();
        testvm6Status.setText("Label");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 2;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(testvm6Status, gbc);
        final JLabel label12 = new JLabel();
        label12.setText("testvm7 :");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(label12, gbc);
        testvm7Status = new JLabel();
        testvm7Status.setText("Label");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 3;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(testvm7Status, gbc);
        final JLabel label13 = new JLabel();
        label13.setText("testvm10 :");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(label13, gbc);
        testvm10Status = new JLabel();
        testvm10Status.setText("Label");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 4;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(testvm10Status, gbc);
        final JLabel label14 = new JLabel();
        label14.setText("testvm11 :");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(label14, gbc);
        testvm11Status = new JLabel();
        testvm11Status.setText("Label");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 5;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(testvm11Status, gbc);
        final JLabel label15 = new JLabel();
        label15.setText("testvm12 :");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(label15, gbc);
        testvm12Status = new JLabel();
        testvm12Status.setText("Label");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 6;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(testvm12Status, gbc);
        final JLabel label16 = new JLabel();
        label16.setText("testvm13 :");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(label16, gbc);
        testvm13Status = new JLabel();
        testvm13Status.setText("Label");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 7;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(testvm13Status, gbc);
        final JLabel label17 = new JLabel();
        label17.setText("testvm14 :");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(label17, gbc);
        testvm14Status = new JLabel();
        testvm14Status.setText("Label");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 8;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(testvm14Status, gbc);
        final JLabel label18 = new JLabel();
        label18.setText("");
        gbc = new GridBagConstraints();
        gbc.gridx = 5;
        gbc.gridy = 10;
        gbc.gridwidth = 2;
        gbc.weightx = 100.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel9.add(label18, gbc);
        refreshButton1 = new JButton();
        refreshButton1.setText("Refresh");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 9;
        gbc.gridwidth = 5;
        gbc.fill = GridBagConstraints.BOTH;
        panel9.add(refreshButton1, gbc);
        final JScrollPane scrollPane4 = new JScrollPane();
        gbc = new GridBagConstraints();
        gbc.gridx = 6;
        gbc.gridy = 0;
        gbc.weighty = 100.0;
        gbc.fill = GridBagConstraints.BOTH;
        panel9.add(scrollPane4, gbc);
        scrollPane4.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(2, 22, 2, 2), null));
        textAreaTestVmStatus = new JTextArea();
        textAreaTestVmStatus.setEditable(false);
        Font textAreaTestVmStatusFont = this.$$$getFont$$$("Source Code Pro Medium", Font.PLAIN, 12, textAreaTestVmStatus.getFont());
        if (textAreaTestVmStatusFont != null) textAreaTestVmStatus.setFont(textAreaTestVmStatusFont);
        scrollPane4.setViewportView(textAreaTestVmStatus);
        final JScrollPane scrollPane5 = new JScrollPane();
        gbc = new GridBagConstraints();
        gbc.gridx = 6;
        gbc.gridy = 1;
        gbc.weighty = 100.0;
        gbc.fill = GridBagConstraints.BOTH;
        panel9.add(scrollPane5, gbc);
        scrollPane5.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(2, 22, 2, 2), null));
        textAreaTestVm3Status = new JTextArea();
        textAreaTestVm3Status.setEditable(false);
        Font textAreaTestVm3StatusFont = this.$$$getFont$$$("Source Code Pro Medium", Font.PLAIN, 12, textAreaTestVm3Status.getFont());
        if (textAreaTestVm3StatusFont != null) textAreaTestVm3Status.setFont(textAreaTestVm3StatusFont);
        scrollPane5.setViewportView(textAreaTestVm3Status);
        final JScrollPane scrollPane6 = new JScrollPane();
        gbc = new GridBagConstraints();
        gbc.gridx = 6;
        gbc.gridy = 2;
        gbc.weighty = 100.0;
        gbc.fill = GridBagConstraints.BOTH;
        panel9.add(scrollPane6, gbc);
        scrollPane6.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(2, 22, 2, 2), null));
        textAreaTestVm6Status = new JTextArea();
        Font textAreaTestVm6StatusFont = this.$$$getFont$$$("Source Code Pro Medium", Font.PLAIN, 12, textAreaTestVm6Status.getFont());
        if (textAreaTestVm6StatusFont != null) textAreaTestVm6Status.setFont(textAreaTestVm6StatusFont);
        scrollPane6.setViewportView(textAreaTestVm6Status);
        final JScrollPane scrollPane7 = new JScrollPane();
        gbc = new GridBagConstraints();
        gbc.gridx = 6;
        gbc.gridy = 3;
        gbc.weighty = 100.0;
        gbc.fill = GridBagConstraints.BOTH;
        panel9.add(scrollPane7, gbc);
        scrollPane7.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(2, 22, 2, 2), null));
        textAreaTestVm7Status = new JTextArea();
        textAreaTestVm7Status.setEditable(false);
        Font textAreaTestVm7StatusFont = this.$$$getFont$$$("Source Code Pro Medium", Font.PLAIN, 12, textAreaTestVm7Status.getFont());
        if (textAreaTestVm7StatusFont != null) textAreaTestVm7Status.setFont(textAreaTestVm7StatusFont);
        scrollPane7.setViewportView(textAreaTestVm7Status);
        final JScrollPane scrollPane8 = new JScrollPane();
        gbc = new GridBagConstraints();
        gbc.gridx = 6;
        gbc.gridy = 4;
        gbc.weighty = 100.0;
        gbc.fill = GridBagConstraints.BOTH;
        panel9.add(scrollPane8, gbc);
        scrollPane8.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(2, 22, 2, 2), null));
        textAreaTestVm10Status = new JTextArea();
        textAreaTestVm10Status.setEditable(false);
        Font textAreaTestVm10StatusFont = this.$$$getFont$$$("Source Code Pro Medium", Font.PLAIN, 12, textAreaTestVm10Status.getFont());
        if (textAreaTestVm10StatusFont != null) textAreaTestVm10Status.setFont(textAreaTestVm10StatusFont);
        scrollPane8.setViewportView(textAreaTestVm10Status);
        final JScrollPane scrollPane9 = new JScrollPane();
        gbc = new GridBagConstraints();
        gbc.gridx = 6;
        gbc.gridy = 5;
        gbc.weighty = 100.0;
        gbc.fill = GridBagConstraints.BOTH;
        panel9.add(scrollPane9, gbc);
        scrollPane9.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(2, 22, 2, 2), null));
        textAreaTestVm11Status = new JTextArea();
        textAreaTestVm11Status.setEditable(false);
        Font textAreaTestVm11StatusFont = this.$$$getFont$$$("Source Code Pro Medium", Font.PLAIN, 12, textAreaTestVm11Status.getFont());
        if (textAreaTestVm11StatusFont != null) textAreaTestVm11Status.setFont(textAreaTestVm11StatusFont);
        scrollPane9.setViewportView(textAreaTestVm11Status);
        final JScrollPane scrollPane10 = new JScrollPane();
        gbc = new GridBagConstraints();
        gbc.gridx = 6;
        gbc.gridy = 6;
        gbc.weighty = 100.0;
        gbc.fill = GridBagConstraints.BOTH;
        panel9.add(scrollPane10, gbc);
        scrollPane10.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(2, 22, 2, 2), null));
        textAreaTestVm12Status = new JTextArea();
        textAreaTestVm12Status.setEditable(false);
        Font textAreaTestVm12StatusFont = this.$$$getFont$$$("Source Code Pro Medium", Font.PLAIN, 12, textAreaTestVm12Status.getFont());
        if (textAreaTestVm12StatusFont != null) textAreaTestVm12Status.setFont(textAreaTestVm12StatusFont);
        scrollPane10.setViewportView(textAreaTestVm12Status);
        final JScrollPane scrollPane11 = new JScrollPane();
        gbc = new GridBagConstraints();
        gbc.gridx = 6;
        gbc.gridy = 7;
        gbc.weighty = 100.0;
        gbc.fill = GridBagConstraints.BOTH;
        panel9.add(scrollPane11, gbc);
        scrollPane11.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(2, 22, 2, 2), null));
        textAreaTestVm13Status = new JTextArea();
        textAreaTestVm13Status.setEditable(false);
        Font textAreaTestVm13StatusFont = this.$$$getFont$$$("Source Code Pro Medium", Font.PLAIN, 12, textAreaTestVm13Status.getFont());
        if (textAreaTestVm13StatusFont != null) textAreaTestVm13Status.setFont(textAreaTestVm13StatusFont);
        scrollPane11.setViewportView(textAreaTestVm13Status);
        final JScrollPane scrollPane12 = new JScrollPane();
        gbc = new GridBagConstraints();
        gbc.gridx = 6;
        gbc.gridy = 8;
        gbc.weighty = 100.0;
        gbc.fill = GridBagConstraints.BOTH;
        panel9.add(scrollPane12, gbc);
        scrollPane12.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(2, 22, 2, 2), null));
        textAreaTestVm14Status = new JTextArea();
        textAreaTestVm14Status.setEditable(false);
        Font textAreaTestVm14StatusFont = this.$$$getFont$$$("Source Code Pro Medium", Font.PLAIN, 12, textAreaTestVm14Status.getFont());
        if (textAreaTestVm14StatusFont != null) textAreaTestVm14Status.setFont(textAreaTestVm14StatusFont);
        scrollPane12.setViewportView(textAreaTestVm14Status);
        panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Clean", panel3);
        cleanTempButton = new JButton();
        cleanTempButton.setText("Clean temp");
        panel3.add(cleanTempButton, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel3.add(spacer1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel1;
    }

    private class CheckAll extends AbstractAction {
        private JPanel panelToCheck;
        private boolean condition;

        CheckAll(JPanel panel, Boolean condition) {
            this.panelToCheck = panel;
            this.condition = condition;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (condition)
                asConfName = "All plugins";
            else
                asConfName = "No plugins";

            boxCleaner(panelToCheck, condition);
        }
    }

    private void boxCleaner(JPanel panelToCheck, Boolean condition) {
        Component[] components = panelToCheck.getComponents();
        for (Component comp : components) {
            if (comp instanceof JCheckBox) {
                JCheckBox box = (JCheckBox) comp;
                box.setSelected(condition);
            }
        }
    }

    private void dotCleaner(JPanel panelToCheck, Boolean condition) {
        Component[] components = panelToCheck.getComponents();
        for (Component comp : components) {
            if (comp instanceof JRadioButton) {
                JRadioButton button = (JRadioButton) comp;
                button.setSelected(false);
            }
        }
    }

    public String getSelectedButtonText(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements(); ) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getText();
            }
        }

        return null;
    }
}


