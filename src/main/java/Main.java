import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        createGUI();
    }

    private static void createGUI() {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    //UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
                    UIManager.setLookAndFeel("mdlaf.MaterialLookAndFeel");
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
                    e.printStackTrace();
                }
                AppFrom appFrom = new AppFrom();
                appFrom.pack();
                appFrom.setTitle("Lazy vm updater");
                appFrom.setSize(850,950);
                appFrom.setVisible(true);
                appFrom.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        });
    }
}